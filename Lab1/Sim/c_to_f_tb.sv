`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/04/2022 12:38:27 AM
// Design Name: 
// Module Name: c_to_f_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module c_to_f_tb( );

logic [7:0] temp_c;
logic [11:0] temp_f;

c_to_f U_DUV(temp_c, temp_f); 

initial begin
        
    temp_c = 8'd28; 
    #10; 
    
    end


endmodule
