`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/08/2022 04:07:15 PM
// Design Name: 
// Module Name: sevenseg_ascii
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevenseg_ascii(input logic [7:0] data,
                      output logic [6:0] segs);


/*
     Seven Seg Output pattern
     A
    F B
     G
    E C
     D     
     
     order is gfedcba
        Bit: 6543210
*/
always_comb begin
    
    case(data)

        //only check for chars we can make on the seven seg
        //For right now only do numbers and capital letters
        8'hFF: segs = 7'b1111111;
        8'h30: segs = 	7'b1000000; //0
        8'h31: segs = 7'b1111001; //1
        8'h32: segs = 7'b0100100;//2
        8'h33: segs = 7'b0110000;//3
        8'h34: segs = 	7'b0011001;//4
        8'h35: segs = 7'b0010010;//5
        8'h36: segs = 7'b0000010;//6
        8'h37: segs = 7'b1111000;//7
        8'h38: segs = 7'b0000000;//8
        8'h39: segs = 7'b0010000;//9
        8'h41: segs =  7'b0001000;//A
        8'h42: segs = 7'b0000011;//B
        8'h43: segs = 	7'b1000110;//C
        8'h44: segs =7'b0100001;//D
        8'h45: segs = 7'b0000110;//E
        8'h46: segs = 7'b0001110; //F
        default: segs = 7'b1111111;

    endcase

end

endmodule
