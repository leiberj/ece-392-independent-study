`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/27/2022 11:58:42 AM
// Design Name: 
// Module Name: uart_receive_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_receive_top(input logic clk, rst,
                        input logic rdy,
                        input logic data,
                        output logic valid, ferr, oerr,
                        output logic [7:0] data_out);


    logic delay_en, rate_clr, en_count_16, clr_count_16, count_16_done, en_count_8, clr_count_8, count_8_done, en_sh_reg, clr_sh_reg, en_sh_reg_output; 

    control_fsm CONTROLLER(.clk(clk), .rst(rst), .data(data), .rdy(rdy), .rate_en(delay_en), .count_16_done(count_16_done), .count_8_done(count_8_done),
                           .valid(valid), .ferr(ferr), .oerr(oerr), .clr_rate_en(rate_clr), .en_count_16(en_count_16), 
                           .clr_count_16(clr_count_16), .en_count_8(en_count_8), .clr_count_8(clr_count_8), 
                           .en_sh_reg(en_sh_reg), .clr_sh_reg(clr_sh_reg), .en_sh_reg_output(en_sh_reg_output)); 

    datapath DATAPATH(.clk(clk), .rst(rst), .rate_clr(rate_clr), .en_count_16(en_count_16), .clr_count_16(clr_count_16), .en_count_8(en_count_8),
                      .clr_count_8(clr_count_8), .clr_sh_reg(clr_sh_reg), .en_shift(en_sh_reg), .en_data_out(en_sh_reg_output), .data(data), 
                      .rate_en_out(delay_en), .count_16_done(count_16_done), .count_8_done(count_8_done), .sh_reg_out(data_out)); 
                    
                        
endmodule
