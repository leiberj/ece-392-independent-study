`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/24/2022 05:02:59 PM
// Design Name: 
// Module Name: sh_reg_en_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sh_reg_en_param #(parameter REG_SIZE = 8) (input logic clk, rst, clr, en_output, en_shift,
                                                  input logic bit_in, 
                                                  output logic [REG_SIZE - 1 : 0] data_out);
    
    logic [REG_SIZE-1 : 0] data_store; 
    integer i; 
    
    always_ff @(posedge clk) begin
        if(rst || clr) begin
            data_store <= '0; 
        end
        else if(en_shift) data_store <= {data_store[REG_SIZE-2:0], bit_in}; 

    end

    always_comb begin
        //Data is being sent little endian, we're processing it as big endian. flip it
        if(en_output) begin
            for(i = 0; i < REG_SIZE; i=i+1) begin
                data_out[i] = data_store[(REG_SIZE - 1) - i];
            end
            //data_out = data_store; 
        end 
        else data_out = '0; 
    end


endmodule
