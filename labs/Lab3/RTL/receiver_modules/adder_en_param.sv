`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/24/2022 04:46:32 PM
// Design Name: 
// Module Name: adder_en_param
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_en_param(input logic clk, rst, en, 
                      input logic clear, 
                      output logic done);

    parameter COUNT = 8;
    //Get the number of bits we'll need to properly add
    localparam COUNT_BITS = $clog2(COUNT); 

    logic [COUNT_BITS-1 : 0] q; 

    assign done = (q == COUNT - 1); 

    always_ff @(posedge clk) begin
        if(rst || clear || (en && done)) q <= '0; 
        else if(en) q <= q + 1'd1; 
    end

endmodule
