`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/24/2022 05:40:18 PM
// Design Name: 
// Module Name: datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath(input logic clk, rst, 
                input logic rate_clr,
                input logic en_count_16, clr_count_16,
                input logic en_count_8, clr_count_8, 
                input logic clr_sh_reg, en_shift, en_data_out, data,

                output logic rate_en_out, 
                output logic count_16_done, 
                output logic count_8_done, 
                output logic [7:0] sh_reg_out);


    //Set the baud rate here
    parameter BAUD_RATE = 9600;
    localparam SAMPLE_RATE = BAUD_RATE << 4; //Sample at 16x the baud rate

    rate_enb #(.RATE_HZ(SAMPLE_RATE)) SAMPLE_RATE_ENABLE(.clk(clk), .rst(rst), .clr(rate_clr), .enb_out(rate_en_out)); 

    //Counter to 16
    adder_en_param #(.COUNT(16)) COUNT_16(.clk(clk), .rst(rst), .en(en_count_16), .clear(clr_count_16), .done(count_16_done)); 

    //Counter to 8
    adder_en_param #(.COUNT(8)) COUNT_8(.clk(clk), .rst(rst), .en(en_count_8), .clear(clr_count_8), .done(count_8_done)); 

    //Shift Register
    sh_reg_en_param #(.REG_SIZE(8)) SHIFT_REGISTER(.clk(clk), .rst(rst), .clr(clr_sh_reg), .en_output(en_data_out), .en_shift(en_shift), .bit_in(data), .data_out(sh_reg_out)); 


endmodule
