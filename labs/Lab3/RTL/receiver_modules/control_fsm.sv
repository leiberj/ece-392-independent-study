`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/27/2022 10:56:43 AM
// Design Name: 
// Module Name: control_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_fsm(input logic clk, rst, 
                   input logic data, rdy,

                   input logic count_16_done, 
                   input logic count_8_done,
                   input logic rate_en,

                   output logic valid, ferr, oerr,
                   output logic clr_rate_en,
                   output logic en_count_16, clr_count_16,
                   output logic en_count_8, clr_count_8,
                   output logic en_sh_reg, clr_sh_reg, en_sh_reg_output);


    //Define states 
    typedef enum logic[2:0]{
        WAIT_FOR_START, WAIT_FOR_START_READY, COUNT_8, CHECK_START, DELAY_16, SHIFT_IN_BIT, CHECK_STOP_DELAY, CHECK_STOP
    }states_t;

    //Create instances of states
    states_t cur_state, next_state; 

    //Default state transitions
    always_ff @(posedge clk) begin
        if(rst) cur_state <= WAIT_FOR_START; 
        else cur_state <= next_state; 
    end

    //Next state and output logic
    always_comb begin      
        //Default values
        valid = 1'b0; 
        ferr = 1'b0; 
        oerr = 1'b0; 
        clr_rate_en = 1'b0; 
        en_count_16 = 1'b0; 
        clr_count_16 = 1'b0; 
        en_count_8 = 1'b0; 
        clr_count_8 = 1'b0; 
        en_sh_reg = 1'b0; 
        clr_sh_reg = 1'b0; 
        en_sh_reg_output = 1'b0; 

        //Next state logic
        case(cur_state) 

            //Wait until we see the falling edge of start bit
            WAIT_FOR_START: begin
                //Start bit is a 0 
                if(!data) next_state = COUNT_8; 
                else if(rdy) next_state = WAIT_FOR_START_READY;
                else next_state = WAIT_FOR_START; 

            end // WAIT_FOR_START:
            
            WAIT_FOR_START_READY: begin
                if(!data) next_state = COUNT_8; 
                else next_state  = WAIT_FOR_START_READY; 
            end

            //Push to center of data cycle
            COUNT_8: begin
                if(count_8_done) next_state = CHECK_START; 
                else next_state = COUNT_8; 
            end // COUNT_8

            //Confirm not glitched start
            CHECK_START: begin
                if(!data) next_state = DELAY_16; 
                else next_state = WAIT_FOR_START; 
            end // CHECK_START:

            //Wait a full data cycle to get center of data bits
            DELAY_16: begin
 
                //We are at the center of the data bit, but we have more data to shift in
                if(count_16_done && rate_en) next_state = SHIFT_IN_BIT; 
                //Delay not done
                else next_state = DELAY_16; 
            end // DELAY_16:

            SHIFT_IN_BIT: begin 
            
                if(!count_8_done) next_state = DELAY_16; 
                else next_state = CHECK_STOP_DELAY; 
            end // SHIFT_IN_BIT:

            CHECK_STOP_DELAY: begin
                if(count_16_done && rate_en) next_state  = CHECK_STOP;
                else next_state  = CHECK_STOP_DELAY;
            end

            CHECK_STOP: begin
                //Handle the outputs in the output stuff
                //That includes the error checking
                if(data) next_state  = WAIT_FOR_START;
                else next_state = CHECK_STOP; 
            end // CHECK_STOP:
            
            default: begin
                next_state = WAIT_FOR_START;
            end

        endcase

        //output logic
        case(cur_state) 

            WAIT_FOR_START: begin
                
                valid = 1;
                en_sh_reg_output = 1'b1; 
            end // WAIT_FOR_START:
            
            WAIT_FOR_START_READY: begin
                valid = 0; 
                clr_count_8 = 1'b1; 
                clr_count_16 = 1'b1; 
                clr_sh_reg = 1'b1;  
            end

            COUNT_8: begin
                if(rate_en) en_count_8 = 1'b1; 

                //Check the overrun error
                //If we still dont see ready at this point through oerr
                if(!(valid == rdy)) oerr = 1'b1; 

            end // COUNT_8

            CHECK_START: begin
                //Reset our 8 counter
                clr_count_8 = 1'b1; 
            end // CHECK_START:

            DELAY_16: begin
                if(rate_en) en_count_16 = 1'b1; 
            end // DELAY_16:

            SHIFT_IN_BIT: begin 
                en_sh_reg = 1'b1; 
                en_count_8 = 1'b1;
            end // SHIFT_IN_BIT:
            
            CHECK_STOP_DELAY: begin
                if(rate_en)  en_count_16 = 1'b1; 
                valid = 1'b1; 
                en_sh_reg_output = 1'b1; 
            end

            CHECK_STOP: begin
                //If we don't see the stop bit, assert ferr
                if(!data) ferr = 1'b1; 
                
                valid = 1'b1; 
                en_sh_reg_output = 1'b1; 
            end // CHECK_STOP:

        endcase

    end

endmodule
