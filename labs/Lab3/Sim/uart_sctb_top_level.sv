`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/03/2022 07:41:46 PM
// Design Name: 
// Module Name: uart_sctb_top_level
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_sctb_top_level();


parameter CLKPD = 10; 

logic clk, rst; 
logic rdy;
logic data;
logic valid, ferr, oerr;
logic [7:0] data_out;

clk_gen #(.CLKPD(CLKPD)) CLOCKGEN(.clk(clk)); 

uart_receive_top DUV(.clk(clk), .rst(rst), .rdy(rdy), .data(data), .valid(valid), .ferr(ferr), .oerr(oerr), .data_out(data_out)); 

uart_receive_top_sctb TB(.clk(clk), .rst(rst), .rdy(rdy), .data(data), .valid(valid), .ferr(ferr), .oerr(oerr), .data_out(data_out)); 

endmodule
