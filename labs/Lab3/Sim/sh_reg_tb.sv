`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/24/2022 05:21:32 PM
// Design Name: 
// Module Name: sh_reg_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sh_reg_tb();

logic clk, rst, clr, en_output, en_shift; 
logic bit_in; 
logic [15:0] data_out; 

initial begin
    
    clk = 1'b0; 
    forever #5 clk = ~clk; 

end 

initial begin
    bit_in  = 1'b0; 
    forever #8 bit_in = ~bit_in; 
end

sh_reg_en_param #(.REG_SIZE(16)) DUV(.clk, .rst, .clr, .en_output, .en_shift, .bit_in, .data_out); 

initial begin
    
    rst = 1;
    #15; 
    rst = 0; 
    clr = 0; 
    en_shift = 1; 
    #5; 
    en_output = 1; 
    #8; 
    clr = 1; 
    #10; 
    clr = 0; 
    en_shift = 1; 
end

endmodule
