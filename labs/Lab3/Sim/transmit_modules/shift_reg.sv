`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 11:28:06 AM
// Design Name: 
// Module Name: shift_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shift_reg(input logic clk, rst, sh_ld, sh_idle, sh_en,
                 input logic start_bit, stop_bit,
                 input logic [7:0] data,
                 output logic txd);

    //Register needs to hold 10 bits (start, data, stop)
    //Shift it at each enable signal
    
    logic [9:0] shift_reg; 
    
    assign txd = shift_reg[0]; 
    
    always_ff @(posedge clk) begin
        if(sh_idle) shift_reg <= 8'hFF; 
        else if(sh_ld) shift_reg <= {stop_bit, data, start_bit};       
        else if(sh_en) shift_reg <= shift_reg >> 1; 
    end
                     
                 
endmodule
