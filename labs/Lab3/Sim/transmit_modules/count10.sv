`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 11:28:06 AM
// Design Name: 
// Module Name: shift_reg
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module count10(input logic clk, rst, ct_clr, ct_en,
               output logic ct_eq_9);
               
               
    logic [3:0] q; 
    
    assign ct_eq_9 = (q == 9);
    
    always_ff @(posedge clk) begin
        
        if(rst || ct_clr ) begin
            q <= 0; 
        end
        else if(ct_en) begin
            if (ct_eq_9) q <= 1'b0; 
            else q <= q + 1'd1;
        end
    end     
          
endmodule
