`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/30/2022 07:57:11 PM
// Design Name: 
// Module Name: decimal_to_whole
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module decimal_to_whole(
    input logic [3:0] decimal,
    output logic [3:0] whole
    );
    
    logic [6:0] mult_by_8; //7 bits
    logic [4:0] mult_by_2; //5 bits
    logic [7:0] sum; //8 bits to store max of the sum
    logic [3:0] out_val; 

    always_comb begin
     
        //Mulitply by 8 
        mult_by_8 = ({3'b000, decimal} << 3);  
        mult_by_2 = ({1'b0, decimal} << 1); 

        //Sum and store in sum
        sum = mult_by_8 + mult_by_2; 
        out_val = sum >> 4; 
    end
    
    assign whole = out_val; 

endmodule
