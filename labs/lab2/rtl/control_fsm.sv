`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 12:08:32 PM
// Design Name: 
// Module Name: control_fsm
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module control_fsm(input logic clk, rst, valid, br_en, ct_eq_9,
                   output logic rdy, 
                   output logic sh_ld, sh_idle, sh_en, 
                   output logic br_st, 
                   output logic ct_clr, ct_en);
                
    typedef enum logic [0:0]{
        WTVALID, TXBIT
    }states_t; 
                 
    states_t cur_state, next_state;              
                   
    always_ff @(posedge clk) begin
        if(rst) cur_state <= WTVALID; 
        else cur_state <= next_state; 
    end                   
    
    
    always_comb begin
        //Default values
        sh_ld = 1'd0; 
        sh_idle = 1'd0; 
        sh_en = 1'd0; 
        br_st = 1'd0; 
        ct_clr = 1'd0; 
        ct_en = 1'd0; 
        
            
        //Next state logic
        case(cur_state) 
                    
            WTVALID: begin
                rdy = 1'b1;
                if(valid) begin
                    sh_ld = 1'd1; 
                    br_st = 1'd1; 
                    ct_clr = 1'd1;
                    next_state = TXBIT; 
                end
                else begin
                    sh_idle = 1'd1; 
                    next_state = WTVALID; 
                end
            end //WTVALID
            
            TXBIT: begin
                rdy = 1'b0;
                if(br_en) begin
                    if(ct_eq_9) begin
                        next_state = WTVALID; 
                    end
                    else begin
                        sh_en = 1'd1; 
                        ct_en = 1'd1;
                        next_state = TXBIT; 
                    end
                end
                else begin
                    next_state = TXBIT; 
                end
                
            end //TXBIT         
        endcase
    end


endmodule
