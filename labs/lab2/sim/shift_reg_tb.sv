`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 11:44:45 AM
// Design Name: 
// Module Name: shift_reg_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module shift_reg_tb();

logic clk, rst, sh_ld, sh_idle, sh_en; 
logic [7:0] data; 
logic txd;

initial begin
    clk = 0; 
    forever #5 clk = ~clk; 
end

shift_reg DUV(clk, rst, sh_ld, sh_idle, sh_en, 1'd0, 1'd1, data, txd); 


initial begin
    
    data = 8'b10101010; 
    sh_idle = 1;
    #50; 
    sh_idle = 0; 
    sh_ld = 1; 
    sh_en = 0; 
    #10; 
    sh_ld = 0; 
    sh_en = 1; 

end

endmodule
