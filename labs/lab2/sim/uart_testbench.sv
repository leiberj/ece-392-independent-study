`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/09/2022 02:01:53 PM
// Design Name: 
// Module Name: uart_testbench
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_testbench( );

logic clk, rst;
logic valid;
logic [7:0] data;
logic rdy;
logic txd;

initial begin
    clk = 1'd0; 
    forever #5 clk = ~clk; 
end

uart_xmit DUV(clk, rst, valid, data, rdy, txd); 

initial begin
    rst = 1'd1; 
    #150; 
    rst = 1'd0; 
    data = 8'b01010101; 
    valid = 1'd1;  
    #2000000;
    valid = 1'd0; 
    #3500000; 
    data = 8'b10011010;
    valid = 1'd1; 
    #200;
    valid = 1'd0; 
     

end

endmodule
