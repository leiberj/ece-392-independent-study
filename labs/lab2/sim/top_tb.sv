`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 12:24:49 PM
// Design Name: 
// Module Name: top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_tb();

logic clk, rst, valid, rdy, txd;
logic [7:0] data; 

initial begin
    clk = 0; 
    forever #5 clk = ~clk; 
end

top DUV(clk, rst, valid, data, rdy, txd); 

initial begin
    
    rst = 1;
    #20; 
    valid = 0;
    rst = 0; 
    #100;
    valid = 1; 
    data = 8'b10101010; 

end

endmodule
