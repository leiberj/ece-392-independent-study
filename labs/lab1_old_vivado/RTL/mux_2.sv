`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/04/2022 01:03:49 AM
// Design Name: 
// Module Name: mux_2
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module mux_2(
    input logic sel,
    input logic [7:0] temp_c, temp_f,
    output logic [3:0] letter,
    output logic [7:0] temp
    );
    
    always_comb begin
        if(sel) begin
            letter = 4'd12; 
            temp = temp_c;
        end
        else begin 
            letter = 4'd15; 
            temp = temp_f; 
        end
    end
    
endmodule
