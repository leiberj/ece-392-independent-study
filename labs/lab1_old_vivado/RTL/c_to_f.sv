`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/31/2022 10:10:18 AM
// Design Name: 
// Module Name: c_to_f
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module c_to_f(
    input logic [7:0] temp_c,
    output logic [11:0] temp_f //Should never fill all of the bits
    );
    
    logic [11:0] shift_4; 
    logic [8:0] shift_1; 
    logic [12:0] temp_f_times_10; 
    
    always_comb begin
        
        shift_4 = temp_c << 4; 
        shift_1 = temp_c << 1; 
        
        
        temp_f_times_10 = (shift_4 + shift_1) + 9'd320; 
        temp_f = temp_f_times_10 / 4'd10;
    end 
       
    
endmodule
