`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04/03/2022 12:34:39 PM
// Design Name: 
// Module Name: top_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_tb();

logic clk, rst; 
logic sclk, blank, lat;
logic [2:0] disp_row, rgb1, rgb2; 

logic sclk_s, blank_s, lat_s, trig_s; 
logic [2:0] rgb2_s; 

//clock gen
initial begin
    clk = 1'b0; 
    forever #5 clk = ~clk; 
end

ledmatrix_top DUV(.clk, .rst, .sclk, .blank, .lat, .disp_row, .rgb1, .rgb2, .sclk_s, .blank_s, .lat_s, .trig_s, .rgb2_s); 

initial begin
    
    rst = 1'b1; 
    #50; 
    rst = 1'b0; 

end

endmodule
