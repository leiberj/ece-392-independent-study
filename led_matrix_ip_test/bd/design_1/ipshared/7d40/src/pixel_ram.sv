`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: Lafayette College
// Engineer: John Nestor
//
// Create Date: 01/10/2019 10:03:33 AM
// Design Name: BRAM storage for adafruit LED matrix
// Module Name: pixel_ram
// Project Name:
// Target Devices:
// Tool Versions:
// Description:
//
// Dependencies:
//
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
//
//////////////////////////////////////////////////////////////////////////////////


module pixel_ram(
  input logic        clk,
  input logic        rst,
  input logic [12:0] rdaddr_pix_upper,
  output logic [3:0] dout_pix_upper,
  input logic [12:0] rdaddr_pix_lower,
  output logic [3:0] dout_pix_lower,
  input logic        we_lower,
  input logic [9:0]  wraddr_col_lower,
  input logic [31:0] din_col_lower
  );

  logic [3:0] we_lower_byte;  // byte write enable
  assign we_lower_byte = { 4 {we_lower} };

  // Upper RAM (read-only)
  BRAM_SDP_MACRO #(
  .BRAM_SIZE("36Kb"), // Target BRAM, "18Kb" or "36Kb"
  .DEVICE("7SERIES"), // Target device: "7SERIES"
  .WRITE_WIDTH(32),    // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
  .READ_WIDTH(4),     // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
  .DO_REG(0),         // Optional output register (0 or 1)
  .INIT_FILE ("NONE"),
  .SIM_COLLISION_CHECK ("ALL"), // Collision check enable "ALL", "WARNING_ONLY",
  //   "GENERATE_X_ONLY" or "NONE"
  .SRVAL(72'h000000000000000000), // Set/Reset value for port output
  .INIT(72'h000000000000000000),  // Initial values on output port
  .WRITE_MODE("WRITE_FIRST"),  // Specify "READ_FIRST" for same clock or synchronous clocks
  //   Specify "WRITE_FIRST for asynchronous clocks on ports

.INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_04(256'h10000004_01226640_00000000_10000004_10006004_10006004_10006004_11226644),
.INIT_05(256'h10006004_10006004_10006004_11226644_00000000_01000040_10000004_10000004),
.INIT_06(256'h10020004_10200004_11000040_00000000_00000000_00000000_00000000_10000004),
.INIT_07(256'h11000040_00000000_10000000_11226644_10000040_00000000_10000640_10006004),
.INIT_08(256'h00000000_00000000_00000000_00000000_10000640_10006004_10020004_10200004)


  /*
  .INIT_04(256'h10000004_01226640_00000000_10000004_10006004_10006004_10006004_11226644),
  .INIT_05(256'h10006004_10006004_10006004_11226644_00000000_01000040_10000004_10000004),
  .INIT_06(256'h10020004_10200004_11000040_00000000_00000000_00000000_00000000_10000004),
  .INIT_07(256'h11000040_00000000_10000000_11226644_10000040_00000000_10000640_10006004),
  .INIT_08(256'h00000000_00000000_00000000_00000000_10000640_10006004_10020004_10200004)*/
  // everything else is blank!

  ) BRAM_UPPER (
  .DO(dout_pix_upper),         // Output read data port, width defined by READ_WIDTH parameter
  .DI(32'd0),         // Input write data port, width defined by WRITE_WIDTH parameter
  .RDADDR(rdaddr_pix_upper), // Input read address, width defined by read port depth
  .RDCLK(clk),   // 1-bit input read clock
  .RDEN(1'b1),     // 1-bit input read port enable
  .REGCE(1'b0),   // 1-bit input read output register enable
  .RST(rst),       // 1-bit input reset
  .WE(4'b0000),         // Input write enable, width defined by write port depth
  .WRADDR(10'd0), // Input write address, width defined by write port depth
  .WRCLK(clk),   // 1-bit input write clock
  .WREN(1'b1)      // 1-bit input write port enable
  );

  // Lower half-matrix RAM
  BRAM_SDP_MACRO #(
  .BRAM_SIZE("36Kb"), // Target BRAM, "18Kb" or "36Kb"
  .DEVICE("7SERIES"), // Target device: "7SERIES"
  .WRITE_WIDTH(32),    // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
  .READ_WIDTH(4),     // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
  .DO_REG(0),         // Optional output register (0 or 1)
  .INIT_FILE ("NONE"),
  .SIM_COLLISION_CHECK ("ALL"), // Collision check enable "ALL", "WARNING_ONLY",
  //   "GENERATE_X_ONLY" or "NONE"
  .SRVAL(72'h000000000000000000), // Set/Reset value for port output
  .INIT(72'h000000000000000000),  // Initial values on output port
  .WRITE_MODE("WRITE_FIRST"),  // Specify "READ_FIRST" for same clock or synchronous clocks
  //   Specify "WRITE_FIRST for asynchronous clocks on ports
  .INIT_00(256'h0000000000000000000000000000000000000000000000000000000000000000),
  .INIT_01(256'h0000000000000000000000000000000000000000000000000000000000000000),
  .INIT_02(256'h0000000000000000000000000000000000000000000000000000000000000000),
  .INIT_03(256'h0000000000000000000000000000000000000000000000000000000000000000),
.INIT_04 (256'h10000004_01226640_00000000_10006004_10006004_10006004_10006004_11226644),
  .INIT_05 (256'h10006004_10006004_10006004_11226644_00000000_01000040_10000004_10000004),
  .INIT_06 (256'h00000000_00000000_00000000_00000000_00000000_00000000_00000000_10006004),
  .INIT_07 (256'h10000040_00000000_00000000_01000040_10000004_10000004_10000004_01226640),
  .INIT_08 (256'h10020600_10020600_10020600_01200000_00000000_00000000_10000000_11226640),
  .INIT_09 (256'h00000000_01000000_10206000_10206000_10206000_10020000_00000000_01206000),
  .INIT_0A (256'h00000000_00000000_00000000_01000000_10206000_10206000_10206000_10020000),
  .INIT_0B (256'h10006000_10006000_10006000_01220000_00000000_00000000_00000000_00000000),
  .INIT_0C (256'h00000000_00000600_00000040_00020040_11226600_00020000_00000000_01220000),
  .INIT_0D (256'h10200004_11000040_00000000_00000000_00000000_00000000_00000000_00000000),
  .INIT_0E (256'h10000604_10006004_10020004_01226640_00000000_10000640_10006004_10020004),
  .INIT_0F (256'h00000000_00000000_10000000_11226644_10000040_00000000_00000000_01226640),
  .INIT_10 (256'h00000000_00000000_00000000_11220640_00006004_00006004_00006004_00000640)


  // everything else is blank!

  ) BRAM_LOWER (
  .DO(dout_pix_lower),         // Output read data port, width defined by READ_WIDTH parameter
  .DI(din_col_lower),         // Input write data port, width defined by WRITE_WIDTH parameter
  .RDADDR(rdaddr_pix_lower), // Input read address, width defined by read port depth
  .RDCLK(clk),   // 1-bit input read clock
  .RDEN(1'b1),     // 1-bit input read port enable
  .REGCE(1'b0),   // 1-bit input read output register enable
  .RST(rst),       // 1-bit input reset
  .WE(we_lower_byte),         // Input write enable, width defined by write port depth
  .WRADDR(wraddr_col_lower), // Input write address, width defined by write port depth
  .WRCLK(clk),   // 1-bit input write clock
  .WREN(1'b1)      // 1-bit input write port enable
  );

endmodule
