-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Thu Apr 21 15:30:03 2022
-- Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub
--               d:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/led_matrix_ip_test/blk_diag/scroll_disp/ip/scroll_disp_ledmatrix_top_0_0/scroll_disp_ledmatrix_top_0_0_stub.vhdl
-- Design      : scroll_disp_ledmatrix_top_0_0
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity scroll_disp_ledmatrix_top_0_0 is
  Port ( 
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    nextbtn : in STD_LOGIC;
    pause_btn : in STD_LOGIC;
    num_color : in STD_LOGIC_VECTOR ( 2 downto 0 );
    sclk : out STD_LOGIC;
    blank : out STD_LOGIC;
    lat : out STD_LOGIC;
    disp_row : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    digit : out STD_LOGIC_VECTOR ( 3 downto 0 )
  );

end scroll_disp_ledmatrix_top_0_0;

architecture stub of scroll_disp_ledmatrix_top_0_0 is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "clk,rst,nextbtn,pause_btn,num_color[2:0],sclk,blank,lat,disp_row[2:0],rgb1[2:0],rgb2[2:0],digit[3:0]";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "ledmatrix_top,Vivado 2020.2";
begin
end;
