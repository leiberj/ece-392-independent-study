//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
//Date        : Thu Apr 21 16:10:31 2022
//Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
//Command     : generate_target scroll_disp_wrapper.bd
//Design      : scroll_disp_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module scroll_disp_wrapper
   (blank,
    digit,
    disp_row,
    lat,
    nextbtn,
    num_color,
    pause_btn,
    pause_btn_tri_o,
    pause_btn_tri_t,
    reset,
    rgb1,
    rgb2,
    sclk,
    sys_clock,
    usb_uart_rxd,
    usb_uart_txd);
  output blank;
  output [3:0]digit;
  output [2:0]disp_row;
  output lat;
  input nextbtn;
  input [2:0]num_color;
  input pause_btn;
  output [0:0]pause_btn_tri_o;
  output [0:0]pause_btn_tri_t;
  input reset;
  output [2:0]rgb1;
  output [2:0]rgb2;
  output sclk;
  input sys_clock;
  input usb_uart_rxd;
  output usb_uart_txd;

  wire blank;
  wire [3:0]digit;
  wire [2:0]disp_row;
  wire lat;
  wire nextbtn;
  wire [2:0]num_color;
  wire pause_btn;
  wire [0:0]pause_btn_tri_o;
  wire [0:0]pause_btn_tri_t;
  wire reset;
  wire [2:0]rgb1;
  wire [2:0]rgb2;
  wire sclk;
  wire sys_clock;
  wire usb_uart_rxd;
  wire usb_uart_txd;

  scroll_disp scroll_disp_i
       (.blank(blank),
        .digit(digit),
        .disp_row(disp_row),
        .lat(lat),
        .nextbtn(nextbtn),
        .num_color(num_color),
        .pause_btn(pause_btn),
        .pause_btn_tri_o(pause_btn_tri_o),
        .pause_btn_tri_t(pause_btn_tri_t),
        .reset(reset),
        .rgb1(rgb1),
        .rgb2(rgb2),
        .sclk(sclk),
        .sys_clock(sys_clock),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd));
endmodule
