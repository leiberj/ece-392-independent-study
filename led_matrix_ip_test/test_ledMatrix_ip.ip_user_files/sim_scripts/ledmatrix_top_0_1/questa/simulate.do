onbreak {quit -f}
onerror {quit -f}

vsim -lib xil_defaultlib ledmatrix_top_0_opt

do {wave.do}

view wave
view structure
view signals

do {ledmatrix_top_0.udo}

run -all

quit -force
