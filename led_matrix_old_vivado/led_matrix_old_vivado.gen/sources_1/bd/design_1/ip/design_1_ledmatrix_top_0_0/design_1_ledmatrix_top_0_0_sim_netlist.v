// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Thu Apr 14 17:45:47 2022
// Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode funcsim
//               d:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/led_matrix_old_vivado/led_matrix_old_vivado.gen/sources_1/bd/design_1/ip/design_1_ledmatrix_top_0_0/design_1_ledmatrix_top_0_0_sim_netlist.v
// Design      : design_1_ledmatrix_top_0_0
// Purpose     : This verilog netlist is a functional simulation representation of the design and should not be modified
//               or synthesized. This netlist cannot be used for SDF annotated simulation.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

(* CHECK_LICENSE_TYPE = "design_1_ledmatrix_top_0_0,ledmatrix_top,{}" *) (* DowngradeIPIdentifiedWarnings = "yes" *) (* IP_DEFINITION_SOURCE = "package_project" *) 
(* X_CORE_INFO = "ledmatrix_top,Vivado 2020.2" *) 
(* NotValidForBitStream *)
module design_1_ledmatrix_top_0_0
   (clk,
    rst,
    sclk,
    blank,
    lat,
    disp_row,
    rgb1,
    rgb2,
    sclk_s,
    blank_s,
    lat_s,
    trig_s,
    rgb2_s);
  (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME clk, ASSOCIATED_RESET rst, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0" *) input clk;
  (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 rst RST" *) (* X_INTERFACE_PARAMETER = "XIL_INTERFACENAME rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0" *) input rst;
  output sclk;
  output blank;
  output lat;
  output [2:0]disp_row;
  output [2:0]rgb1;
  output [2:0]rgb2;
  output sclk_s;
  output blank_s;
  output lat_s;
  output trig_s;
  output [2:0]rgb2_s;

  wire \<const0> ;
  wire blank;
  wire blank_s;
  wire clk;
  wire [2:0]disp_row;
  wire lat;
  wire lat_s;
  wire [2:0]rgb1;
  wire [2:0]rgb2;
  wire [2:0]rgb2_s;
  wire rst;
  wire sclk;
  wire sclk_s;
  wire NLW_inst_trig_s_UNCONNECTED;

  assign trig_s = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ledmatrix_top_0_0_ledmatrix_top inst
       (.blank(blank),
        .blank_s(blank_s),
        .clk(clk),
        .disp_row(disp_row),
        .lat(lat),
        .lat_s(lat_s),
        .rgb1(rgb1),
        .rgb2(rgb2),
        .rgb2_s(rgb2_s),
        .rst(rst),
        .sclk(sclk),
        .sclk_s(sclk_s),
        .trig_s(NLW_inst_trig_s_UNCONNECTED));
endmodule

(* ORIG_REF_NAME = "colctr" *) 
module design_1_ledmatrix_top_0_0_colctr
   (\col_reg[5]_0 ,
    AR,
    \col_reg[5]_1 ,
    E,
    D,
    \col_reg[0]_0 ,
    \col_reg[2]_0 ,
    AS,
    \col_reg[5]_2 ,
    \FSM_onehot_state_reg[2] ,
    Q,
    \col_reg[0]_1 ,
    \rgb2[0] ,
    \rgb1[0] ,
    \rgb1[1] ,
    \rgb2_reg[0]_i_1_0 ,
    \rgb2[2] ,
    SR,
    clk);
  output \col_reg[5]_0 ;
  output [1:0]AR;
  output [0:0]\col_reg[5]_1 ;
  output [0:0]E;
  output [2:0]D;
  output \col_reg[0]_0 ;
  output \col_reg[2]_0 ;
  output [0:0]AS;
  output [1:0]\col_reg[5]_2 ;
  output \FSM_onehot_state_reg[2] ;
  input [0:0]Q;
  input \col_reg[0]_1 ;
  input \rgb2[0] ;
  input \rgb1[0] ;
  input \rgb1[1] ;
  input \rgb2_reg[0]_i_1_0 ;
  input \rgb2[2] ;
  input [0:0]SR;
  input clk;

  wire [1:0]AR;
  wire [0:0]AS;
  wire [2:0]D;
  wire [0:0]E;
  wire \FSM_onehot_state[1]_i_3_n_0 ;
  wire \FSM_onehot_state_reg[2] ;
  wire [0:0]Q;
  wire [0:0]SR;
  wire clk;
  wire [4:0]col;
  wire \col[0]_i_1_n_0 ;
  wire \col[5]_i_2_n_0 ;
  wire \col_reg[0]_0 ;
  wire \col_reg[0]_1 ;
  wire \col_reg[2]_0 ;
  wire \col_reg[5]_0 ;
  wire [0:0]\col_reg[5]_1 ;
  wire [1:0]\col_reg[5]_2 ;
  wire [5:1]p_0_in__0;
  wire \rgb1[0] ;
  wire \rgb1[1] ;
  wire \rgb1_reg[0]_i_5_n_0 ;
  wire \rgb1_reg[0]_i_6_n_0 ;
  wire \rgb1_reg[1]_i_4_n_0 ;
  wire \rgb1_reg[2]_i_3_n_0 ;
  wire \rgb1_reg[2]_i_4_n_0 ;
  wire \rgb1_reg[2]_i_7_n_0 ;
  wire \rgb2[0] ;
  wire \rgb2[2] ;
  wire \rgb2_reg[0]_i_1_0 ;
  wire \rgb2_reg[0]_i_2_n_0 ;
  wire \rgb2_reg[2]_i_3_n_0 ;

  LUT6 #(
    .INIT(64'h2AAAAAAAAAAAAAAA)) 
    \FSM_onehot_state[1]_i_2 
       (.I0(Q),
        .I1(\FSM_onehot_state[1]_i_3_n_0 ),
        .I2(col[0]),
        .I3(col[1]),
        .I4(col[4]),
        .I5(\col_reg[5]_1 ),
        .O(\FSM_onehot_state_reg[2] ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \FSM_onehot_state[1]_i_3 
       (.I0(col[3]),
        .I1(col[2]),
        .O(\FSM_onehot_state[1]_i_3_n_0 ));
  LUT6 #(
    .INIT(64'h7FFFFFFFFFFFFFFF)) 
    \FSM_onehot_state[3]_i_2 
       (.I0(\col_reg[5]_1 ),
        .I1(col[4]),
        .I2(col[1]),
        .I3(col[0]),
        .I4(col[3]),
        .I5(col[2]),
        .O(\col_reg[5]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \col[0]_i_1 
       (.I0(col[0]),
        .O(\col[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \col[1]_i_1 
       (.I0(col[1]),
        .I1(col[0]),
        .O(p_0_in__0[1]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \col[2]_i_1 
       (.I0(col[2]),
        .I1(col[0]),
        .I2(col[1]),
        .O(p_0_in__0[2]));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT4 #(
    .INIT(16'h6AAA)) 
    \col[3]_i_1 
       (.I0(col[3]),
        .I1(col[0]),
        .I2(col[2]),
        .I3(col[1]),
        .O(p_0_in__0[3]));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \col[4]_i_1 
       (.I0(col[4]),
        .I1(col[1]),
        .I2(col[0]),
        .I3(col[3]),
        .I4(col[2]),
        .O(p_0_in__0[4]));
  LUT3 #(
    .INIT(8'h80)) 
    \col[5]_i_2 
       (.I0(\col_reg[5]_0 ),
        .I1(Q),
        .I2(\col_reg[0]_1 ),
        .O(\col[5]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \col[5]_i_3 
       (.I0(\col_reg[5]_1 ),
        .I1(col[3]),
        .I2(col[4]),
        .I3(col[1]),
        .I4(col[2]),
        .I5(col[0]),
        .O(p_0_in__0[5]));
  FDRE \col_reg[0] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(\col[0]_i_1_n_0 ),
        .Q(col[0]),
        .R(SR));
  FDRE \col_reg[1] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(p_0_in__0[1]),
        .Q(col[1]),
        .R(SR));
  FDRE \col_reg[2] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(p_0_in__0[2]),
        .Q(col[2]),
        .R(SR));
  FDRE \col_reg[3] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(p_0_in__0[3]),
        .Q(col[3]),
        .R(SR));
  FDRE \col_reg[4] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(p_0_in__0[4]),
        .Q(col[4]),
        .R(SR));
  FDRE \col_reg[5] 
       (.C(clk),
        .CE(\col[5]_i_2_n_0 ),
        .D(p_0_in__0[5]),
        .Q(\col_reg[5]_1 ),
        .R(SR));
  LUT6 #(
    .INIT(64'h0000020000004000)) 
    \rgb1_reg[0]_i_1 
       (.I0(\col_reg[5]_1 ),
        .I1(col[4]),
        .I2(col[3]),
        .I3(col[1]),
        .I4(col[0]),
        .I5(col[2]),
        .O(D[0]));
  LUT6 #(
    .INIT(64'hD7F7F7F7F7F7F777)) 
    \rgb1_reg[0]_i_2 
       (.I0(\rgb1[0] ),
        .I1(col[4]),
        .I2(col[3]),
        .I3(col[2]),
        .I4(col[1]),
        .I5(col[0]),
        .O(E));
  LUT5 #(
    .INIT(32'hFFFFAABA)) 
    \rgb1_reg[0]_i_3 
       (.I0(\rgb1_reg[0]_i_5_n_0 ),
        .I1(\col_reg[5]_1 ),
        .I2(\rgb2[0] ),
        .I3(\rgb1_reg[1]_i_4_n_0 ),
        .I4(\rgb1_reg[0]_i_6_n_0 ),
        .O(AR[0]));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT5 #(
    .INIT(32'hAAAEAEAE)) 
    \rgb1_reg[0]_i_5 
       (.I0(AS),
        .I1(\rgb1[1] ),
        .I2(col[4]),
        .I3(col[2]),
        .I4(col[3]),
        .O(\rgb1_reg[0]_i_5_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT5 #(
    .INIT(32'h00000008)) 
    \rgb1_reg[0]_i_6 
       (.I0(col[2]),
        .I1(col[3]),
        .I2(col[0]),
        .I3(col[4]),
        .I4(\col_reg[5]_1 ),
        .O(\rgb1_reg[0]_i_6_n_0 ));
  LUT6 #(
    .INIT(64'hF444F444FFFFF444)) 
    \rgb1_reg[1]_i_1 
       (.I0(\col_reg[0]_0 ),
        .I1(\rgb2[0] ),
        .I2(\col_reg[2]_0 ),
        .I3(col[1]),
        .I4(\rgb1[1] ),
        .I5(\rgb1_reg[1]_i_4_n_0 ),
        .O(D[1]));
  LUT6 #(
    .INIT(64'h3300320CCFFFFFFF)) 
    \rgb1_reg[1]_i_2 
       (.I0(col[0]),
        .I1(\col_reg[5]_1 ),
        .I2(col[2]),
        .I3(col[3]),
        .I4(col[1]),
        .I5(col[4]),
        .O(\col_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h8000002210000000)) 
    \rgb1_reg[1]_i_3 
       (.I0(col[2]),
        .I1(col[0]),
        .I2(col[1]),
        .I3(col[4]),
        .I4(\col_reg[5]_1 ),
        .I5(col[3]),
        .O(\col_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT5 #(
    .INIT(32'hAA7FA87F)) 
    \rgb1_reg[1]_i_4 
       (.I0(col[3]),
        .I1(col[2]),
        .I2(col[1]),
        .I3(col[4]),
        .I4(col[0]),
        .O(\rgb1_reg[1]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'hFEAAAAAAAAAAAAAA)) 
    \rgb1_reg[2]_i_1 
       (.I0(\rgb1_reg[2]_i_3_n_0 ),
        .I1(col[3]),
        .I2(\rgb1_reg[2]_i_4_n_0 ),
        .I3(\rgb2[0] ),
        .I4(\col_reg[5]_1 ),
        .I5(col[4]),
        .O(D[2]));
  LUT6 #(
    .INIT(64'h0700070007009F99)) 
    \rgb1_reg[2]_i_2 
       (.I0(col[3]),
        .I1(col[2]),
        .I2(col[4]),
        .I3(\rgb1[1] ),
        .I4(col[1]),
        .I5(\rgb1_reg[2]_i_7_n_0 ),
        .O(AR[1]));
  LUT6 #(
    .INIT(64'h8000000010200000)) 
    \rgb1_reg[2]_i_3 
       (.I0(col[2]),
        .I1(col[0]),
        .I2(col[1]),
        .I3(col[4]),
        .I4(\col_reg[5]_1 ),
        .I5(col[3]),
        .O(\rgb1_reg[2]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \rgb1_reg[2]_i_4 
       (.I0(col[2]),
        .I1(col[1]),
        .O(\rgb1_reg[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \rgb1_reg[2]_i_7 
       (.I0(\col_reg[5]_1 ),
        .I1(col[4]),
        .I2(col[0]),
        .O(\rgb1_reg[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFEAAAAAAA)) 
    \rgb2_reg[0]_i_1 
       (.I0(\rgb2_reg[0]_i_2_n_0 ),
        .I1(\rgb1[1] ),
        .I2(\rgb1_reg[2]_i_4_n_0 ),
        .I3(col[3]),
        .I4(col[4]),
        .I5(D[0]),
        .O(\col_reg[5]_2 [0]));
  LUT6 #(
    .INIT(64'h0303030303030113)) 
    \rgb2_reg[0]_i_2 
       (.I0(col[0]),
        .I1(\rgb2_reg[0]_i_1_0 ),
        .I2(col[4]),
        .I3(col[2]),
        .I4(col[1]),
        .I5(col[3]),
        .O(\rgb2_reg[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'hAAEAAAEAFFFFAAEA)) 
    \rgb2_reg[2]_i_1 
       (.I0(\rgb1_reg[2]_i_3_n_0 ),
        .I1(\rgb2_reg[2]_i_3_n_0 ),
        .I2(\col_reg[5]_1 ),
        .I3(\rgb2[2] ),
        .I4(\rgb1_reg[0]_i_6_n_0 ),
        .I5(col[1]),
        .O(\col_reg[5]_2 [1]));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \rgb2_reg[2]_i_2 
       (.I0(col[2]),
        .I1(col[1]),
        .I2(col[3]),
        .I3(col[0]),
        .I4(col[4]),
        .I5(\col_reg[5]_1 ),
        .O(AS));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT5 #(
    .INIT(32'hFFFFFCC4)) 
    \rgb2_reg[2]_i_3 
       (.I0(col[0]),
        .I1(col[4]),
        .I2(col[2]),
        .I3(col[1]),
        .I4(col[3]),
        .O(\rgb2_reg[2]_i_3_n_0 ));
endmodule

(* ORIG_REF_NAME = "delayunit" *) 
module design_1_ledmatrix_top_0_0_delayunit
   (sel,
    \q_reg[0]_0 ,
    clk);
  output sel;
  input \q_reg[0]_0 ;
  input clk;

  wire clk;
  wire \q[0]_i_4_n_0 ;
  wire \q[0]_i_5_n_0 ;
  wire \q[0]_i_6_n_0 ;
  wire \q[0]_i_7_n_0 ;
  wire \q[0]_i_8_n_0 ;
  wire \q[0]_i_9_n_0 ;
  wire [18:0]q_reg;
  wire \q_reg[0]_0 ;
  wire \q_reg[0]_i_3_n_0 ;
  wire \q_reg[0]_i_3_n_1 ;
  wire \q_reg[0]_i_3_n_2 ;
  wire \q_reg[0]_i_3_n_3 ;
  wire \q_reg[0]_i_3_n_4 ;
  wire \q_reg[0]_i_3_n_5 ;
  wire \q_reg[0]_i_3_n_6 ;
  wire \q_reg[0]_i_3_n_7 ;
  wire \q_reg[12]_i_1_n_0 ;
  wire \q_reg[12]_i_1_n_1 ;
  wire \q_reg[12]_i_1_n_2 ;
  wire \q_reg[12]_i_1_n_3 ;
  wire \q_reg[12]_i_1_n_4 ;
  wire \q_reg[12]_i_1_n_5 ;
  wire \q_reg[12]_i_1_n_6 ;
  wire \q_reg[12]_i_1_n_7 ;
  wire \q_reg[16]_i_1_n_2 ;
  wire \q_reg[16]_i_1_n_3 ;
  wire \q_reg[16]_i_1_n_5 ;
  wire \q_reg[16]_i_1_n_6 ;
  wire \q_reg[16]_i_1_n_7 ;
  wire \q_reg[4]_i_1_n_0 ;
  wire \q_reg[4]_i_1_n_1 ;
  wire \q_reg[4]_i_1_n_2 ;
  wire \q_reg[4]_i_1_n_3 ;
  wire \q_reg[4]_i_1_n_4 ;
  wire \q_reg[4]_i_1_n_5 ;
  wire \q_reg[4]_i_1_n_6 ;
  wire \q_reg[4]_i_1_n_7 ;
  wire \q_reg[8]_i_1_n_0 ;
  wire \q_reg[8]_i_1_n_1 ;
  wire \q_reg[8]_i_1_n_2 ;
  wire \q_reg[8]_i_1_n_3 ;
  wire \q_reg[8]_i_1_n_4 ;
  wire \q_reg[8]_i_1_n_5 ;
  wire \q_reg[8]_i_1_n_6 ;
  wire \q_reg[8]_i_1_n_7 ;
  wire sel;
  wire [3:2]\NLW_q_reg[16]_i_1_CO_UNCONNECTED ;
  wire [3:3]\NLW_q_reg[16]_i_1_O_UNCONNECTED ;

  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \q[0]_i_2 
       (.I0(\q[0]_i_4_n_0 ),
        .I1(\q[0]_i_5_n_0 ),
        .I2(\q[0]_i_6_n_0 ),
        .I3(\q[0]_i_7_n_0 ),
        .I4(\q[0]_i_8_n_0 ),
        .O(sel));
  LUT4 #(
    .INIT(16'hFFDF)) 
    \q[0]_i_4 
       (.I0(q_reg[9]),
        .I1(q_reg[17]),
        .I2(q_reg[6]),
        .I3(q_reg[14]),
        .O(\q[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hDFFF)) 
    \q[0]_i_5 
       (.I0(q_reg[8]),
        .I1(q_reg[5]),
        .I2(q_reg[3]),
        .I3(q_reg[4]),
        .O(\q[0]_i_5_n_0 ));
  LUT4 #(
    .INIT(16'h7FFF)) 
    \q[0]_i_6 
       (.I0(q_reg[0]),
        .I1(q_reg[12]),
        .I2(q_reg[18]),
        .I3(q_reg[1]),
        .O(\q[0]_i_6_n_0 ));
  LUT4 #(
    .INIT(16'hFFFD)) 
    \q[0]_i_7 
       (.I0(q_reg[7]),
        .I1(q_reg[13]),
        .I2(q_reg[16]),
        .I3(q_reg[11]),
        .O(\q[0]_i_7_n_0 ));
  LUT3 #(
    .INIT(8'hDF)) 
    \q[0]_i_8 
       (.I0(q_reg[2]),
        .I1(q_reg[10]),
        .I2(q_reg[15]),
        .O(\q[0]_i_8_n_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \q[0]_i_9 
       (.I0(q_reg[0]),
        .O(\q[0]_i_9_n_0 ));
  FDRE \q_reg[0] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[0]_i_3_n_7 ),
        .Q(q_reg[0]),
        .R(\q_reg[0]_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[0]_i_3 
       (.CI(1'b0),
        .CO({\q_reg[0]_i_3_n_0 ,\q_reg[0]_i_3_n_1 ,\q_reg[0]_i_3_n_2 ,\q_reg[0]_i_3_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({\q_reg[0]_i_3_n_4 ,\q_reg[0]_i_3_n_5 ,\q_reg[0]_i_3_n_6 ,\q_reg[0]_i_3_n_7 }),
        .S({q_reg[3:1],\q[0]_i_9_n_0 }));
  FDRE \q_reg[10] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[8]_i_1_n_5 ),
        .Q(q_reg[10]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[11] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[8]_i_1_n_4 ),
        .Q(q_reg[11]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[12] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[12]_i_1_n_7 ),
        .Q(q_reg[12]),
        .R(\q_reg[0]_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[12]_i_1 
       (.CI(\q_reg[8]_i_1_n_0 ),
        .CO({\q_reg[12]_i_1_n_0 ,\q_reg[12]_i_1_n_1 ,\q_reg[12]_i_1_n_2 ,\q_reg[12]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[12]_i_1_n_4 ,\q_reg[12]_i_1_n_5 ,\q_reg[12]_i_1_n_6 ,\q_reg[12]_i_1_n_7 }),
        .S(q_reg[15:12]));
  FDRE \q_reg[13] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[12]_i_1_n_6 ),
        .Q(q_reg[13]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[14] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[12]_i_1_n_5 ),
        .Q(q_reg[14]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[15] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[12]_i_1_n_4 ),
        .Q(q_reg[15]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[16] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[16]_i_1_n_7 ),
        .Q(q_reg[16]),
        .R(\q_reg[0]_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[16]_i_1 
       (.CI(\q_reg[12]_i_1_n_0 ),
        .CO({\NLW_q_reg[16]_i_1_CO_UNCONNECTED [3:2],\q_reg[16]_i_1_n_2 ,\q_reg[16]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\NLW_q_reg[16]_i_1_O_UNCONNECTED [3],\q_reg[16]_i_1_n_5 ,\q_reg[16]_i_1_n_6 ,\q_reg[16]_i_1_n_7 }),
        .S({1'b0,q_reg[18:16]}));
  FDRE \q_reg[17] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[16]_i_1_n_6 ),
        .Q(q_reg[17]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[18] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[16]_i_1_n_5 ),
        .Q(q_reg[18]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[1] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[0]_i_3_n_6 ),
        .Q(q_reg[1]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[2] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[0]_i_3_n_5 ),
        .Q(q_reg[2]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[3] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[0]_i_3_n_4 ),
        .Q(q_reg[3]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[4] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[4]_i_1_n_7 ),
        .Q(q_reg[4]),
        .R(\q_reg[0]_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[4]_i_1 
       (.CI(\q_reg[0]_i_3_n_0 ),
        .CO({\q_reg[4]_i_1_n_0 ,\q_reg[4]_i_1_n_1 ,\q_reg[4]_i_1_n_2 ,\q_reg[4]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[4]_i_1_n_4 ,\q_reg[4]_i_1_n_5 ,\q_reg[4]_i_1_n_6 ,\q_reg[4]_i_1_n_7 }),
        .S(q_reg[7:4]));
  FDRE \q_reg[5] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[4]_i_1_n_6 ),
        .Q(q_reg[5]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[6] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[4]_i_1_n_5 ),
        .Q(q_reg[6]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[7] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[4]_i_1_n_4 ),
        .Q(q_reg[7]),
        .R(\q_reg[0]_0 ));
  FDRE \q_reg[8] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[8]_i_1_n_7 ),
        .Q(q_reg[8]),
        .R(\q_reg[0]_0 ));
  (* ADDER_THRESHOLD = "11" *) 
  CARRY4 \q_reg[8]_i_1 
       (.CI(\q_reg[4]_i_1_n_0 ),
        .CO({\q_reg[8]_i_1_n_0 ,\q_reg[8]_i_1_n_1 ,\q_reg[8]_i_1_n_2 ,\q_reg[8]_i_1_n_3 }),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({\q_reg[8]_i_1_n_4 ,\q_reg[8]_i_1_n_5 ,\q_reg[8]_i_1_n_6 ,\q_reg[8]_i_1_n_7 }),
        .S(q_reg[11:8]));
  FDRE \q_reg[9] 
       (.C(clk),
        .CE(sel),
        .D(\q_reg[8]_i_1_n_6 ),
        .Q(q_reg[9]),
        .R(\q_reg[0]_0 ));
endmodule

(* ORIG_REF_NAME = "dffr" *) 
module design_1_ledmatrix_top_0_0_dffr
   (lat,
    rst,
    Q,
    clk);
  output lat;
  input rst;
  input [0:0]Q;
  input clk;

  wire [0:0]Q;
  wire clk;
  wire lat;
  wire rst;

  FDRE q_reg
       (.C(clk),
        .CE(1'b1),
        .D(Q),
        .Q(lat),
        .R(rst));
endmodule

(* ORIG_REF_NAME = "ledmatrix_top" *) 
module design_1_ledmatrix_top_0_0_ledmatrix_top
   (clk,
    rst,
    sclk,
    blank,
    lat,
    disp_row,
    rgb1,
    rgb2,
    sclk_s,
    blank_s,
    lat_s,
    trig_s,
    rgb2_s);
  input clk;
  input rst;
  output sclk;
  output blank;
  output lat;
  output [2:0]disp_row;
  output [2:0]rgb1;
  output [2:0]rgb2;
  output sclk_s;
  output blank_s;
  output lat_s;
  output trig_s;
  output [2:0]rgb2_s;

  wire \<const0> ;
  wire U_SEQ_n_10;
  wire U_SEQ_n_11;
  wire U_SEQ_n_12;
  wire U_SEQ_n_13;
  wire U_SEQ_n_14;
  wire U_SEQ_n_5;
  wire U_SEQ_n_6;
  wire U_SEQ_n_7;
  wire U_SEQ_n_8;
  wire U_SEQ_n_9;
  wire blank_s;
  wire clk;
  wire [2:0]disp_row;
  wire lat;
  wire [2:0]rgb1;
  wire [2:0]rgb2;
  wire rst;
  wire sclk;

  assign blank = blank_s;
  assign lat_s = lat;
  assign rgb2_s[2:0] = rgb2;
  assign sclk_s = sclk;
  assign trig_s = \<const0> ;
  GND GND
       (.G(\<const0> ));
  design_1_ledmatrix_top_0_0_pixel_generator U_PIX
       (.AR({U_SEQ_n_5,U_SEQ_n_6}),
        .AS(U_SEQ_n_11),
        .D({U_SEQ_n_8,U_SEQ_n_9,U_SEQ_n_10}),
        .E(U_SEQ_n_7),
        .rgb1(rgb1),
        .rgb2(rgb2),
        .\rgb2[2] ({U_SEQ_n_12,U_SEQ_n_13,U_SEQ_n_14}));
  design_1_ledmatrix_top_0_0_sequencer U_SEQ
       (.AR({U_SEQ_n_5,U_SEQ_n_6}),
        .AS(U_SEQ_n_11),
        .D({U_SEQ_n_8,U_SEQ_n_9,U_SEQ_n_10}),
        .E(U_SEQ_n_7),
        .blank_s(blank_s),
        .clk(clk),
        .\col_reg[5] ({U_SEQ_n_12,U_SEQ_n_13,U_SEQ_n_14}),
        .disp_row(disp_row),
        .lat(lat),
        .rst(rst),
        .sclk(sclk));
endmodule

(* ORIG_REF_NAME = "period_enb" *) 
module design_1_ledmatrix_top_0_0_period_enb
   (\q_reg[3]_0 ,
    rst,
    clk);
  output \q_reg[3]_0 ;
  input rst;
  input clk;

  wire clk;
  wire [5:0]p_0_in;
  wire \q[5]_i_1_n_0 ;
  wire [5:0]q_reg;
  wire \q_reg[3]_0 ;
  wire rst;

  LUT6 #(
    .INIT(64'h0100000000000000)) 
    \disp_row[2]_i_2 
       (.I0(q_reg[3]),
        .I1(q_reg[2]),
        .I2(q_reg[1]),
        .I3(q_reg[5]),
        .I4(q_reg[4]),
        .I5(q_reg[0]),
        .O(\q_reg[3]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \q[0]_i_1__0 
       (.I0(q_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \q[1]_i_1 
       (.I0(q_reg[1]),
        .I1(q_reg[0]),
        .O(p_0_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \q[2]_i_1 
       (.I0(q_reg[2]),
        .I1(q_reg[1]),
        .I2(q_reg[0]),
        .O(p_0_in[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT4 #(
    .INIT(16'h7F80)) 
    \q[3]_i_1 
       (.I0(q_reg[0]),
        .I1(q_reg[1]),
        .I2(q_reg[2]),
        .I3(q_reg[3]),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT5 #(
    .INIT(32'h6AAAAAAA)) 
    \q[4]_i_1 
       (.I0(q_reg[4]),
        .I1(q_reg[0]),
        .I2(q_reg[1]),
        .I3(q_reg[2]),
        .I4(q_reg[3]),
        .O(p_0_in[4]));
  LUT2 #(
    .INIT(4'hE)) 
    \q[5]_i_1 
       (.I0(rst),
        .I1(\q_reg[3]_0 ),
        .O(\q[5]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h6AAAAAAAAAAAAAAA)) 
    \q[5]_i_2 
       (.I0(q_reg[5]),
        .I1(q_reg[3]),
        .I2(q_reg[2]),
        .I3(q_reg[1]),
        .I4(q_reg[0]),
        .I5(q_reg[4]),
        .O(p_0_in[5]));
  FDRE \q_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[0]),
        .Q(q_reg[0]),
        .R(\q[5]_i_1_n_0 ));
  FDRE \q_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[1]),
        .Q(q_reg[1]),
        .R(\q[5]_i_1_n_0 ));
  FDRE \q_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[2]),
        .Q(q_reg[2]),
        .R(\q[5]_i_1_n_0 ));
  FDRE \q_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[3]),
        .Q(q_reg[3]),
        .R(\q[5]_i_1_n_0 ));
  FDRE \q_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[4]),
        .Q(q_reg[4]),
        .R(\q[5]_i_1_n_0 ));
  FDRE \q_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(p_0_in[5]),
        .Q(q_reg[5]),
        .R(\q[5]_i_1_n_0 ));
endmodule

(* ORIG_REF_NAME = "pixel_generator" *) 
module design_1_ledmatrix_top_0_0_pixel_generator
   (rgb1,
    rgb2,
    D,
    AR,
    E,
    \rgb2[2] ,
    AS);
  output [2:0]rgb1;
  output [2:0]rgb2;
  input [2:0]D;
  input [1:0]AR;
  input [0:0]E;
  input [2:0]\rgb2[2] ;
  input [0:0]AS;

  wire [1:0]AR;
  wire [0:0]AS;
  wire [2:0]D;
  wire [0:0]E;
  wire [2:0]rgb1;
  wire [2:0]rgb2;
  wire [2:0]\rgb2[2] ;

  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \rgb1_reg[0] 
       (.CLR(AR[0]),
        .D(D[0]),
        .G(E),
        .GE(1'b1),
        .Q(rgb1[0]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \rgb1_reg[1] 
       (.CLR(AR[1]),
        .D(D[1]),
        .G(1'b1),
        .GE(1'b1),
        .Q(rgb1[1]));
  (* XILINX_LEGACY_PRIM = "LDP" *) 
  LDPE #(
    .INIT(1'b1)) 
    \rgb1_reg[2] 
       (.D(D[2]),
        .G(1'b1),
        .GE(1'b1),
        .PRE(AR[1]),
        .Q(rgb1[2]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \rgb2_reg[0] 
       (.CLR(AR[0]),
        .D(\rgb2[2] [0]),
        .G(E),
        .GE(1'b1),
        .Q(rgb2[0]));
  (* XILINX_LEGACY_PRIM = "LDC" *) 
  LDCE #(
    .INIT(1'b0)) 
    \rgb2_reg[1] 
       (.CLR(AR[1]),
        .D(\rgb2[2] [1]),
        .G(1'b1),
        .GE(1'b1),
        .Q(rgb2[1]));
  (* XILINX_LEGACY_PRIM = "LDP" *) 
  LDPE #(
    .INIT(1'b1)) 
    \rgb2_reg[2] 
       (.D(\rgb2[2] [2]),
        .G(1'b1),
        .GE(1'b1),
        .PRE(AS),
        .Q(rgb2[2]));
endmodule

(* ORIG_REF_NAME = "rowctr" *) 
module design_1_ledmatrix_top_0_0_rowctr
   (\row_reg[2]_0 ,
    \col_reg[5] ,
    \row_reg[2]_1 ,
    \row_reg[0]_0 ,
    \row_reg[1]_0 ,
    \row_reg[0]_1 ,
    disp_row,
    \rgb2_reg[0]_i_2 ,
    \rgb2[1] ,
    \rgb2[1]_0 ,
    rst,
    E,
    clk);
  output \row_reg[2]_0 ;
  output \col_reg[5] ;
  output \row_reg[2]_1 ;
  output \row_reg[0]_0 ;
  output \row_reg[1]_0 ;
  output [0:0]\row_reg[0]_1 ;
  output [2:0]disp_row;
  input [0:0]\rgb2_reg[0]_i_2 ;
  input \rgb2[1] ;
  input \rgb2[1]_0 ;
  input rst;
  input [0:0]E;
  input clk;

  wire [0:0]E;
  wire clk;
  wire \col_reg[5] ;
  wire [2:0]disp_row;
  wire [1:0]p_1_in;
  wire \rgb2[1] ;
  wire \rgb2[1]_0 ;
  wire [0:0]\rgb2_reg[0]_i_2 ;
  wire \row[2]_i_1_n_0 ;
  wire [2:0]row__0;
  wire \row_reg[0]_0 ;
  wire [0:0]\row_reg[0]_1 ;
  wire \row_reg[1]_0 ;
  wire \row_reg[2]_0 ;
  wire \row_reg[2]_1 ;
  wire rst;

  FDSE \disp_row_reg[0] 
       (.C(clk),
        .CE(E),
        .D(row__0[0]),
        .Q(disp_row[0]),
        .S(rst));
  FDSE \disp_row_reg[1] 
       (.C(clk),
        .CE(E),
        .D(row__0[1]),
        .Q(disp_row[1]),
        .S(rst));
  FDSE \disp_row_reg[2] 
       (.C(clk),
        .CE(E),
        .D(row__0[2]),
        .Q(disp_row[2]),
        .S(rst));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'h0001)) 
    \rgb1_reg[0]_i_4 
       (.I0(row__0[1]),
        .I1(row__0[0]),
        .I2(row__0[2]),
        .I3(\rgb2_reg[0]_i_2 ),
        .O(\row_reg[1]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \rgb1_reg[2]_i_5 
       (.I0(row__0[2]),
        .I1(row__0[0]),
        .I2(row__0[1]),
        .O(\row_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    \rgb1_reg[2]_i_6 
       (.I0(\rgb2_reg[0]_i_2 ),
        .I1(row__0[2]),
        .I2(row__0[1]),
        .I3(row__0[0]),
        .O(\col_reg[5] ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT4 #(
    .INIT(16'h7FFF)) 
    \rgb2_reg[0]_i_3 
       (.I0(row__0[2]),
        .I1(row__0[1]),
        .I2(row__0[0]),
        .I3(\rgb2_reg[0]_i_2 ),
        .O(\row_reg[2]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT5 #(
    .INIT(32'hBAAAAAAA)) 
    \rgb2_reg[1]_i_1 
       (.I0(\rgb2[1] ),
        .I1(\rgb2[1]_0 ),
        .I2(row__0[0]),
        .I3(row__0[1]),
        .I4(row__0[2]),
        .O(\row_reg[0]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \rgb2_reg[2]_i_4 
       (.I0(row__0[0]),
        .I1(row__0[1]),
        .I2(row__0[2]),
        .O(\row_reg[0]_0 ));
  LUT1 #(
    .INIT(2'h1)) 
    \row[0]_i_1 
       (.I0(row__0[0]),
        .O(p_1_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \row[1]_i_1 
       (.I0(row__0[1]),
        .I1(row__0[0]),
        .O(p_1_in[1]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT3 #(
    .INIT(8'h6A)) 
    \row[2]_i_1 
       (.I0(row__0[2]),
        .I1(row__0[0]),
        .I2(row__0[1]),
        .O(\row[2]_i_1_n_0 ));
  FDRE \row_reg[0] 
       (.C(clk),
        .CE(E),
        .D(p_1_in[0]),
        .Q(row__0[0]),
        .R(rst));
  FDRE \row_reg[1] 
       (.C(clk),
        .CE(E),
        .D(p_1_in[1]),
        .Q(row__0[1]),
        .R(rst));
  FDRE \row_reg[2] 
       (.C(clk),
        .CE(E),
        .D(\row[2]_i_1_n_0 ),
        .Q(row__0[2]),
        .R(rst));
endmodule

(* ORIG_REF_NAME = "sclk_r" *) 
module design_1_ledmatrix_top_0_0_sclk_r
   (sclk,
    sclk_reg_0,
    clk);
  output sclk;
  input sclk_reg_0;
  input clk;

  wire clk;
  wire sclk;
  wire sclk_reg_0;

  FDRE sclk_reg
       (.C(clk),
        .CE(1'b1),
        .D(sclk_reg_0),
        .Q(sclk),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "sequencer" *) 
module design_1_ledmatrix_top_0_0_sequencer
   (lat,
    sclk,
    disp_row,
    AR,
    E,
    D,
    AS,
    \col_reg[5] ,
    blank_s,
    rst,
    clk);
  output lat;
  output sclk;
  output [2:0]disp_row;
  output [1:0]AR;
  output [0:0]E;
  output [2:0]D;
  output [0:0]AS;
  output [2:0]\col_reg[5] ;
  output blank_s;
  input rst;
  input clk;

  wire [1:0]AR;
  wire [0:0]AS;
  wire [2:0]D;
  wire [0:0]E;
  wire U_COLCTR_n_0;
  wire U_COLCTR_n_13;
  wire U_COLCTR_n_8;
  wire U_COLCTR_n_9;
  wire U_HALFSCLK_n_0;
  wire U_ROWCTR_n_0;
  wire U_ROWCTR_n_1;
  wire U_ROWCTR_n_2;
  wire U_ROWCTR_n_3;
  wire U_ROWCTR_n_4;
  wire U_SEQ_n_3;
  wire U_SEQ_n_4;
  wire U_SEQ_n_5;
  wire blank_s;
  wire clk;
  wire [5:5]col;
  wire [2:0]\col_reg[5] ;
  wire [2:0]disp_row;
  wire lat;
  wire lat_c;
  wire rowct_enb;
  wire rst;
  wire sclk;
  wire sclk_r_set;
  wire sel;

  design_1_ledmatrix_top_0_0_colctr U_COLCTR
       (.AR(AR),
        .AS(AS),
        .D(D),
        .E(E),
        .\FSM_onehot_state_reg[2] (U_COLCTR_n_13),
        .Q(sclk_r_set),
        .SR(U_SEQ_n_5),
        .clk(clk),
        .\col_reg[0]_0 (U_COLCTR_n_8),
        .\col_reg[0]_1 (U_HALFSCLK_n_0),
        .\col_reg[2]_0 (U_COLCTR_n_9),
        .\col_reg[5]_0 (U_COLCTR_n_0),
        .\col_reg[5]_1 (col),
        .\col_reg[5]_2 ({\col_reg[5] [2],\col_reg[5] [0]}),
        .\rgb1[0] (U_ROWCTR_n_4),
        .\rgb1[1] (U_ROWCTR_n_1),
        .\rgb2[0] (U_ROWCTR_n_0),
        .\rgb2[2] (U_ROWCTR_n_3),
        .\rgb2_reg[0]_i_1_0 (U_ROWCTR_n_2));
  design_1_ledmatrix_top_0_0_dffr U_DFF
       (.Q(lat_c),
        .clk(clk),
        .lat(lat),
        .rst(rst));
  design_1_ledmatrix_top_0_0_period_enb U_HALFSCLK
       (.clk(clk),
        .\q_reg[3]_0 (U_HALFSCLK_n_0),
        .rst(rst));
  design_1_ledmatrix_top_0_0_delayunit U_REFKDLY
       (.clk(clk),
        .\q_reg[0]_0 (U_SEQ_n_3),
        .sel(sel));
  design_1_ledmatrix_top_0_0_rowctr U_ROWCTR
       (.E(rowct_enb),
        .clk(clk),
        .\col_reg[5] (U_ROWCTR_n_1),
        .disp_row(disp_row),
        .\rgb2[1] (U_COLCTR_n_9),
        .\rgb2[1]_0 (U_COLCTR_n_8),
        .\rgb2_reg[0]_i_2 (col),
        .\row_reg[0]_0 (U_ROWCTR_n_3),
        .\row_reg[0]_1 (\col_reg[5] [1]),
        .\row_reg[1]_0 (U_ROWCTR_n_4),
        .\row_reg[2]_0 (U_ROWCTR_n_0),
        .\row_reg[2]_1 (U_ROWCTR_n_2),
        .rst(rst));
  design_1_ledmatrix_top_0_0_sclk_r U_SCLK_R
       (.clk(clk),
        .sclk(sclk),
        .sclk_reg_0(U_SEQ_n_4));
  design_1_ledmatrix_top_0_0_sequencer_fsm U_SEQ
       (.E(rowct_enb),
        .\FSM_onehot_state_reg[1]_0 (U_ROWCTR_n_0),
        .\FSM_onehot_state_reg[1]_1 (U_COLCTR_n_13),
        .\FSM_onehot_state_reg[3]_0 (U_COLCTR_n_0),
        .\FSM_onehot_state_reg[3]_1 (U_HALFSCLK_n_0),
        .Q({lat_c,sclk_r_set}),
        .SR(U_SEQ_n_5),
        .blank_s(blank_s),
        .clk(clk),
        .rst(rst),
        .rst_0(U_SEQ_n_3),
        .rst_1(U_SEQ_n_4),
        .sclk(sclk),
        .sel(sel));
endmodule

(* ORIG_REF_NAME = "sequencer_fsm" *) 
module design_1_ledmatrix_top_0_0_sequencer_fsm
   (Q,
    E,
    rst_0,
    rst_1,
    SR,
    blank_s,
    \FSM_onehot_state_reg[3]_0 ,
    \FSM_onehot_state_reg[3]_1 ,
    \FSM_onehot_state_reg[1]_0 ,
    sel,
    \FSM_onehot_state_reg[1]_1 ,
    rst,
    sclk,
    clk);
  output [1:0]Q;
  output [0:0]E;
  output rst_0;
  output rst_1;
  output [0:0]SR;
  output blank_s;
  input \FSM_onehot_state_reg[3]_0 ;
  input \FSM_onehot_state_reg[3]_1 ;
  input \FSM_onehot_state_reg[1]_0 ;
  input sel;
  input \FSM_onehot_state_reg[1]_1 ;
  input rst;
  input sclk;
  input clk;

  wire [0:0]E;
  wire \FSM_onehot_state[0]_i_1_n_0 ;
  wire \FSM_onehot_state[1]_i_1_n_0 ;
  wire \FSM_onehot_state[2]_i_1_n_0 ;
  wire \FSM_onehot_state[3]_i_1_n_0 ;
  wire \FSM_onehot_state[4]_i_1_n_0 ;
  wire \FSM_onehot_state[5]_i_1_n_0 ;
  wire \FSM_onehot_state_reg[1]_0 ;
  wire \FSM_onehot_state_reg[1]_1 ;
  wire \FSM_onehot_state_reg[3]_0 ;
  wire \FSM_onehot_state_reg[3]_1 ;
  wire \FSM_onehot_state_reg_n_0_[0] ;
  wire \FSM_onehot_state_reg_n_0_[1] ;
  wire \FSM_onehot_state_reg_n_0_[3] ;
  wire \FSM_onehot_state_reg_n_0_[4] ;
  wire [1:0]Q;
  wire [0:0]SR;
  wire blank_s;
  wire clk;
  wire rst;
  wire rst_0;
  wire rst_1;
  wire sclk;
  wire sel;

  LUT5 #(
    .INIT(32'hFFF080F0)) 
    \FSM_onehot_state[0]_i_1 
       (.I0(\FSM_onehot_state_reg[1]_0 ),
        .I1(sel),
        .I2(\FSM_onehot_state_reg_n_0_[0] ),
        .I3(\FSM_onehot_state_reg[3]_1 ),
        .I4(Q[1]),
        .O(\FSM_onehot_state[0]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFF002A2AFF00)) 
    \FSM_onehot_state[1]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[0] ),
        .I1(\FSM_onehot_state_reg[1]_0 ),
        .I2(sel),
        .I3(\FSM_onehot_state_reg_n_0_[1] ),
        .I4(\FSM_onehot_state_reg[3]_1 ),
        .I5(\FSM_onehot_state_reg[1]_1 ),
        .O(\FSM_onehot_state[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_onehot_state[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[1] ),
        .I1(\FSM_onehot_state_reg[3]_1 ),
        .I2(Q[0]),
        .O(\FSM_onehot_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'h2F20)) 
    \FSM_onehot_state[3]_i_1 
       (.I0(Q[0]),
        .I1(\FSM_onehot_state_reg[3]_0 ),
        .I2(\FSM_onehot_state_reg[3]_1 ),
        .I3(\FSM_onehot_state_reg_n_0_[3] ),
        .O(\FSM_onehot_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_onehot_state[4]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[3] ),
        .I1(\FSM_onehot_state_reg[3]_1 ),
        .I2(\FSM_onehot_state_reg_n_0_[4] ),
        .O(\FSM_onehot_state[4]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hB8)) 
    \FSM_onehot_state[5]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[4] ),
        .I1(\FSM_onehot_state_reg[3]_1 ),
        .I2(Q[1]),
        .O(\FSM_onehot_state[5]_i_1_n_0 ));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDSE #(
    .INIT(1'b1)) 
    \FSM_onehot_state_reg[0] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[0]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[0] ),
        .S(rst));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[1] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[1]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[1] ),
        .R(rst));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[2] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[2]_i_1_n_0 ),
        .Q(Q[0]),
        .R(rst));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[3] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[3]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[3] ),
        .R(rst));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[4] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[4]_i_1_n_0 ),
        .Q(\FSM_onehot_state_reg_n_0_[4] ),
        .R(rst));
  (* FSM_ENCODED_STATES = "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010" *) 
  FDRE #(
    .INIT(1'b0)) 
    \FSM_onehot_state_reg[5] 
       (.C(clk),
        .CE(1'b1),
        .D(\FSM_onehot_state[5]_i_1_n_0 ),
        .Q(Q[1]),
        .R(rst));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    blank_s_INST_0
       (.I0(\FSM_onehot_state_reg_n_0_[0] ),
        .I1(\FSM_onehot_state_reg_n_0_[4] ),
        .I2(Q[1]),
        .O(blank_s));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \col[5]_i_1 
       (.I0(rst),
        .I1(\FSM_onehot_state_reg_n_0_[4] ),
        .I2(\FSM_onehot_state_reg_n_0_[3] ),
        .O(SR));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h8)) 
    \disp_row[2]_i_1 
       (.I0(\FSM_onehot_state_reg_n_0_[3] ),
        .I1(\FSM_onehot_state_reg[3]_1 ),
        .O(E));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \q[0]_i_1 
       (.I0(rst),
        .I1(Q[1]),
        .O(rst_0));
  LUT6 #(
    .INIT(64'h0000000001010100)) 
    sclk_i_1
       (.I0(rst),
        .I1(\FSM_onehot_state_reg_n_0_[4] ),
        .I2(\FSM_onehot_state_reg_n_0_[3] ),
        .I3(Q[0]),
        .I4(sclk),
        .I5(\FSM_onehot_state_reg_n_0_[1] ),
        .O(rst_1));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif
