// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Thu Apr 14 17:45:47 2022
// Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               d:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/led_matrix_old_vivado/led_matrix_old_vivado.gen/sources_1/bd/design_1/ip/design_1_ledmatrix_top_0_0/design_1_ledmatrix_top_0_0_stub.v
// Design      : design_1_ledmatrix_top_0_0
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "ledmatrix_top,Vivado 2020.2" *)
module design_1_ledmatrix_top_0_0(clk, rst, sclk, blank, lat, disp_row, rgb1, rgb2, sclk_s, 
  blank_s, lat_s, trig_s, rgb2_s)
/* synthesis syn_black_box black_box_pad_pin="clk,rst,sclk,blank,lat,disp_row[2:0],rgb1[2:0],rgb2[2:0],sclk_s,blank_s,lat_s,trig_s,rgb2_s[2:0]" */;
  input clk;
  input rst;
  output sclk;
  output blank;
  output lat;
  output [2:0]disp_row;
  output [2:0]rgb1;
  output [2:0]rgb2;
  output sclk_s;
  output blank_s;
  output lat_s;
  output trig_s;
  output [2:0]rgb2_s;
endmodule
