//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
//Date        : Thu Apr 14 17:52:16 2022
//Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
//Command     : generate_target design_1_wrapper.bd
//Design      : design_1_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module design_1_wrapper
   (blank,
    blank_s,
    disp_row,
    lat,
    lat_s,
    reset,
    rgb1,
    rgb2,
    rgb2_s,
    sclk,
    sclk_s,
    sys_clock,
    trig_s,
    usb_uart_rxd,
    usb_uart_txd);
  output blank;
  output blank_s;
  output [2:0]disp_row;
  output lat;
  output lat_s;
  input reset;
  output [2:0]rgb1;
  output [2:0]rgb2;
  output [2:0]rgb2_s;
  output sclk;
  output sclk_s;
  input sys_clock;
  output trig_s;
  input usb_uart_rxd;
  output usb_uart_txd;

  wire blank;
  wire blank_s;
  wire [2:0]disp_row;
  wire lat;
  wire lat_s;
  wire reset;
  wire [2:0]rgb1;
  wire [2:0]rgb2;
  wire [2:0]rgb2_s;
  wire sclk;
  wire sclk_s;
  wire sys_clock;
  wire trig_s;
  wire usb_uart_rxd;
  wire usb_uart_txd;

  design_1 design_1_i
       (.blank(blank),
        .blank_s(blank_s),
        .disp_row(disp_row),
        .lat(lat),
        .lat_s(lat_s),
        .reset(reset),
        .rgb1(rgb1),
        .rgb2(rgb2),
        .rgb2_s(rgb2_s),
        .sclk(sclk),
        .sclk_s(sclk_s),
        .sys_clock(sys_clock),
        .trig_s(trig_s),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd));
endmodule
