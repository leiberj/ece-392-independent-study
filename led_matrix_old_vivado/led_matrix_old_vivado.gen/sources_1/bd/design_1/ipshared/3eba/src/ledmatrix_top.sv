module ledmatrix_top (
    input logic        clk, rst,
    output  logic      sclk, blank, lat,
    output logic [2:0] disp_row, rgb1, rgb2,
    output logic       sclk_s, blank_s, lat_s, trig_s,
    output logic [2:0] rgb2_s
    );

   logic [2:0] row;
   logic [5:0] col;


   sequencer U_SEQ(.clk, .rst, .col, .row, .disp_row, .sclk, .blank, .lat, .trig(trig_s));

   pixel_generator U_PIX(.col, .row, .rgb1, .rgb2);

//  frame_buf U_FRAMEBUF (
//      .clk, .rst, .rdcol(col), .rdrow(row),
//      .rgb_upper(rgb1), .rgb_lower(rgb2),
//      .we_upper(1'b0), .we_lower(1'b0), .wrcol(5'b0), .din_col(32'd0)
//      );


// extra output for scope
assign sclk_s = sclk;
assign lat_s = lat;
assign blank_s = blank;
assign rgb2_s = rgb2;

endmodule // ledmatrix_top
