-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Thu Apr 14 17:45:45 2022
-- Host        : DESKTOP-RS82AEF running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode funcsim -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ design_1_ledmatrix_top_0_0_sim_netlist.vhdl
-- Design      : design_1_ledmatrix_top_0_0
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colctr is
  port (
    \col_reg[5]_0\ : out STD_LOGIC;
    AR : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \col_reg[5]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \col_reg[0]_0\ : out STD_LOGIC;
    \col_reg[2]_0\ : out STD_LOGIC;
    AS : out STD_LOGIC_VECTOR ( 0 to 0 );
    \col_reg[5]_2\ : out STD_LOGIC_VECTOR ( 1 downto 0 );
    \FSM_onehot_state_reg[2]\ : out STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    \col_reg[0]_1\ : in STD_LOGIC;
    \rgb2[0]\ : in STD_LOGIC;
    \rgb1[0]\ : in STD_LOGIC;
    \rgb1[1]\ : in STD_LOGIC;
    \rgb2_reg[0]_i_1_0\ : in STD_LOGIC;
    \rgb2[2]\ : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colctr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colctr is
  signal \^as\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \^d\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \FSM_onehot_state[1]_i_3_n_0\ : STD_LOGIC;
  signal col : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \col[0]_i_1_n_0\ : STD_LOGIC;
  signal \col[5]_i_2_n_0\ : STD_LOGIC;
  signal \^col_reg[0]_0\ : STD_LOGIC;
  signal \^col_reg[2]_0\ : STD_LOGIC;
  signal \^col_reg[5]_0\ : STD_LOGIC;
  signal \^col_reg[5]_1\ : STD_LOGIC_VECTOR ( 0 to 0 );
  signal \p_0_in__0\ : STD_LOGIC_VECTOR ( 5 downto 1 );
  signal \rgb1_reg[0]_i_5_n_0\ : STD_LOGIC;
  signal \rgb1_reg[0]_i_6_n_0\ : STD_LOGIC;
  signal \rgb1_reg[1]_i_4_n_0\ : STD_LOGIC;
  signal \rgb1_reg[2]_i_3_n_0\ : STD_LOGIC;
  signal \rgb1_reg[2]_i_4_n_0\ : STD_LOGIC;
  signal \rgb1_reg[2]_i_7_n_0\ : STD_LOGIC;
  signal \rgb2_reg[0]_i_2_n_0\ : STD_LOGIC;
  signal \rgb2_reg[2]_i_3_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[1]_i_3\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \col[1]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \col[2]_i_1\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \col[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \col[4]_i_1\ : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \rgb1_reg[0]_i_5\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \rgb1_reg[0]_i_6\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \rgb1_reg[1]_i_4\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \rgb1_reg[2]_i_7\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \rgb2_reg[2]_i_3\ : label is "soft_lutpair0";
begin
  AS(0) <= \^as\(0);
  D(2 downto 0) <= \^d\(2 downto 0);
  \col_reg[0]_0\ <= \^col_reg[0]_0\;
  \col_reg[2]_0\ <= \^col_reg[2]_0\;
  \col_reg[5]_0\ <= \^col_reg[5]_0\;
  \col_reg[5]_1\(0) <= \^col_reg[5]_1\(0);
\FSM_onehot_state[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"2AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => Q(0),
      I1 => \FSM_onehot_state[1]_i_3_n_0\,
      I2 => col(0),
      I3 => col(1),
      I4 => col(4),
      I5 => \^col_reg[5]_1\(0),
      O => \FSM_onehot_state_reg[2]\
    );
\FSM_onehot_state[1]_i_3\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => col(3),
      I1 => col(2),
      O => \FSM_onehot_state[1]_i_3_n_0\
    );
\FSM_onehot_state[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"7FFFFFFFFFFFFFFF"
    )
        port map (
      I0 => \^col_reg[5]_1\(0),
      I1 => col(4),
      I2 => col(1),
      I3 => col(0),
      I4 => col(3),
      I5 => col(2),
      O => \^col_reg[5]_0\
    );
\col[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => col(0),
      O => \col[0]_i_1_n_0\
    );
\col[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => col(1),
      I1 => col(0),
      O => \p_0_in__0\(1)
    );
\col[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => col(2),
      I1 => col(0),
      I2 => col(1),
      O => \p_0_in__0\(2)
    );
\col[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6AAA"
    )
        port map (
      I0 => col(3),
      I1 => col(0),
      I2 => col(2),
      I3 => col(1),
      O => \p_0_in__0\(3)
    );
\col[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => col(4),
      I1 => col(1),
      I2 => col(0),
      I3 => col(3),
      I4 => col(2),
      O => \p_0_in__0\(4)
    );
\col[5]_i_2\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"80"
    )
        port map (
      I0 => \^col_reg[5]_0\,
      I1 => Q(0),
      I2 => \col_reg[0]_1\,
      O => \col[5]_i_2_n_0\
    );
\col[5]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \^col_reg[5]_1\(0),
      I1 => col(3),
      I2 => col(4),
      I3 => col(1),
      I4 => col(2),
      I5 => col(0),
      O => \p_0_in__0\(5)
    );
\col_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \col[0]_i_1_n_0\,
      Q => col(0),
      R => SR(0)
    );
\col_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \p_0_in__0\(1),
      Q => col(1),
      R => SR(0)
    );
\col_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \p_0_in__0\(2),
      Q => col(2),
      R => SR(0)
    );
\col_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \p_0_in__0\(3),
      Q => col(3),
      R => SR(0)
    );
\col_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \p_0_in__0\(4),
      Q => col(4),
      R => SR(0)
    );
\col_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \col[5]_i_2_n_0\,
      D => \p_0_in__0\(5),
      Q => \^col_reg[5]_1\(0),
      R => SR(0)
    );
\rgb1_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000020000004000"
    )
        port map (
      I0 => \^col_reg[5]_1\(0),
      I1 => col(4),
      I2 => col(3),
      I3 => col(1),
      I4 => col(0),
      I5 => col(2),
      O => \^d\(0)
    );
\rgb1_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"D7F7F7F7F7F7F777"
    )
        port map (
      I0 => \rgb1[0]\,
      I1 => col(4),
      I2 => col(3),
      I3 => col(2),
      I4 => col(1),
      I5 => col(0),
      O => E(0)
    );
\rgb1_reg[0]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFAABA"
    )
        port map (
      I0 => \rgb1_reg[0]_i_5_n_0\,
      I1 => \^col_reg[5]_1\(0),
      I2 => \rgb2[0]\,
      I3 => \rgb1_reg[1]_i_4_n_0\,
      I4 => \rgb1_reg[0]_i_6_n_0\,
      O => AR(0)
    );
\rgb1_reg[0]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AAAEAEAE"
    )
        port map (
      I0 => \^as\(0),
      I1 => \rgb1[1]\,
      I2 => col(4),
      I3 => col(2),
      I4 => col(3),
      O => \rgb1_reg[0]_i_5_n_0\
    );
\rgb1_reg[0]_i_6\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00000008"
    )
        port map (
      I0 => col(2),
      I1 => col(3),
      I2 => col(0),
      I3 => col(4),
      I4 => \^col_reg[5]_1\(0),
      O => \rgb1_reg[0]_i_6_n_0\
    );
\rgb1_reg[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F444F444FFFFF444"
    )
        port map (
      I0 => \^col_reg[0]_0\,
      I1 => \rgb2[0]\,
      I2 => \^col_reg[2]_0\,
      I3 => col(1),
      I4 => \rgb1[1]\,
      I5 => \rgb1_reg[1]_i_4_n_0\,
      O => \^d\(1)
    );
\rgb1_reg[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3300320CCFFFFFFF"
    )
        port map (
      I0 => col(0),
      I1 => \^col_reg[5]_1\(0),
      I2 => col(2),
      I3 => col(3),
      I4 => col(1),
      I5 => col(4),
      O => \^col_reg[0]_0\
    );
\rgb1_reg[1]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000002210000000"
    )
        port map (
      I0 => col(2),
      I1 => col(0),
      I2 => col(1),
      I3 => col(4),
      I4 => \^col_reg[5]_1\(0),
      I5 => col(3),
      O => \^col_reg[2]_0\
    );
\rgb1_reg[1]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"AA7FA87F"
    )
        port map (
      I0 => col(3),
      I1 => col(2),
      I2 => col(1),
      I3 => col(4),
      I4 => col(0),
      O => \rgb1_reg[1]_i_4_n_0\
    );
\rgb1_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FEAAAAAAAAAAAAAA"
    )
        port map (
      I0 => \rgb1_reg[2]_i_3_n_0\,
      I1 => col(3),
      I2 => \rgb1_reg[2]_i_4_n_0\,
      I3 => \rgb2[0]\,
      I4 => \^col_reg[5]_1\(0),
      I5 => col(4),
      O => \^d\(2)
    );
\rgb1_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0700070007009F99"
    )
        port map (
      I0 => col(3),
      I1 => col(2),
      I2 => col(4),
      I3 => \rgb1[1]\,
      I4 => col(1),
      I5 => \rgb1_reg[2]_i_7_n_0\,
      O => AR(1)
    );
\rgb1_reg[2]_i_3\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8000000010200000"
    )
        port map (
      I0 => col(2),
      I1 => col(0),
      I2 => col(1),
      I3 => col(4),
      I4 => \^col_reg[5]_1\(0),
      I5 => col(3),
      O => \rgb1_reg[2]_i_3_n_0\
    );
\rgb1_reg[2]_i_4\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => col(2),
      I1 => col(1),
      O => \rgb1_reg[2]_i_4_n_0\
    );
\rgb1_reg[2]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^col_reg[5]_1\(0),
      I1 => col(4),
      I2 => col(0),
      O => \rgb1_reg[2]_i_7_n_0\
    );
\rgb2_reg[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAAAAAA"
    )
        port map (
      I0 => \rgb2_reg[0]_i_2_n_0\,
      I1 => \rgb1[1]\,
      I2 => \rgb1_reg[2]_i_4_n_0\,
      I3 => col(3),
      I4 => col(4),
      I5 => \^d\(0),
      O => \col_reg[5]_2\(0)
    );
\rgb2_reg[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0303030303030113"
    )
        port map (
      I0 => col(0),
      I1 => \rgb2_reg[0]_i_1_0\,
      I2 => col(4),
      I3 => col(2),
      I4 => col(1),
      I5 => col(3),
      O => \rgb2_reg[0]_i_2_n_0\
    );
\rgb2_reg[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AAEAAAEAFFFFAAEA"
    )
        port map (
      I0 => \rgb1_reg[2]_i_3_n_0\,
      I1 => \rgb2_reg[2]_i_3_n_0\,
      I2 => \^col_reg[5]_1\(0),
      I3 => \rgb2[2]\,
      I4 => \rgb1_reg[0]_i_6_n_0\,
      I5 => col(1),
      O => \col_reg[5]_2\(1)
    );
\rgb2_reg[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000000000001"
    )
        port map (
      I0 => col(2),
      I1 => col(1),
      I2 => col(3),
      I3 => col(0),
      I4 => col(4),
      I5 => \^col_reg[5]_1\(0),
      O => \^as\(0)
    );
\rgb2_reg[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFCC4"
    )
        port map (
      I0 => col(0),
      I1 => col(4),
      I2 => col(2),
      I3 => col(1),
      I4 => col(3),
      O => \rgb2_reg[2]_i_3_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delayunit is
  port (
    sel : out STD_LOGIC;
    \q_reg[0]_0\ : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delayunit;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delayunit is
  signal \q[0]_i_4_n_0\ : STD_LOGIC;
  signal \q[0]_i_5_n_0\ : STD_LOGIC;
  signal \q[0]_i_6_n_0\ : STD_LOGIC;
  signal \q[0]_i_7_n_0\ : STD_LOGIC;
  signal \q[0]_i_8_n_0\ : STD_LOGIC;
  signal \q[0]_i_9_n_0\ : STD_LOGIC;
  signal q_reg : STD_LOGIC_VECTOR ( 18 downto 0 );
  signal \q_reg[0]_i_3_n_0\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_1\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_2\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_3\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_4\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_5\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_6\ : STD_LOGIC;
  signal \q_reg[0]_i_3_n_7\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[12]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[16]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[4]_i_1_n_7\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_0\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_1\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_2\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_3\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_4\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_5\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_6\ : STD_LOGIC;
  signal \q_reg[8]_i_1_n_7\ : STD_LOGIC;
  signal \^sel\ : STD_LOGIC;
  signal \NLW_q_reg[16]_i_1_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_q_reg[16]_i_1_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \q_reg[0]_i_3\ : label is 11;
  attribute ADDER_THRESHOLD of \q_reg[12]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \q_reg[16]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \q_reg[4]_i_1\ : label is 11;
  attribute ADDER_THRESHOLD of \q_reg[8]_i_1\ : label is 11;
begin
  sel <= \^sel\;
\q[0]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => \q[0]_i_4_n_0\,
      I1 => \q[0]_i_5_n_0\,
      I2 => \q[0]_i_6_n_0\,
      I3 => \q[0]_i_7_n_0\,
      I4 => \q[0]_i_8_n_0\,
      O => \^sel\
    );
\q[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFDF"
    )
        port map (
      I0 => q_reg(9),
      I1 => q_reg(17),
      I2 => q_reg(6),
      I3 => q_reg(14),
      O => \q[0]_i_4_n_0\
    );
\q[0]_i_5\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DFFF"
    )
        port map (
      I0 => q_reg(8),
      I1 => q_reg(5),
      I2 => q_reg(3),
      I3 => q_reg(4),
      O => \q[0]_i_5_n_0\
    );
\q[0]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => q_reg(0),
      I1 => q_reg(12),
      I2 => q_reg(18),
      I3 => q_reg(1),
      O => \q[0]_i_6_n_0\
    );
\q[0]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFD"
    )
        port map (
      I0 => q_reg(7),
      I1 => q_reg(13),
      I2 => q_reg(16),
      I3 => q_reg(11),
      O => \q[0]_i_7_n_0\
    );
\q[0]_i_8\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"DF"
    )
        port map (
      I0 => q_reg(2),
      I1 => q_reg(10),
      I2 => q_reg(15),
      O => \q[0]_i_8_n_0\
    );
\q[0]_i_9\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => q_reg(0),
      O => \q[0]_i_9_n_0\
    );
\q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[0]_i_3_n_7\,
      Q => q_reg(0),
      R => \q_reg[0]_0\
    );
\q_reg[0]_i_3\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \q_reg[0]_i_3_n_0\,
      CO(2) => \q_reg[0]_i_3_n_1\,
      CO(1) => \q_reg[0]_i_3_n_2\,
      CO(0) => \q_reg[0]_i_3_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0001",
      O(3) => \q_reg[0]_i_3_n_4\,
      O(2) => \q_reg[0]_i_3_n_5\,
      O(1) => \q_reg[0]_i_3_n_6\,
      O(0) => \q_reg[0]_i_3_n_7\,
      S(3 downto 1) => q_reg(3 downto 1),
      S(0) => \q[0]_i_9_n_0\
    );
\q_reg[10]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[8]_i_1_n_5\,
      Q => q_reg(10),
      R => \q_reg[0]_0\
    );
\q_reg[11]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[8]_i_1_n_4\,
      Q => q_reg(11),
      R => \q_reg[0]_0\
    );
\q_reg[12]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[12]_i_1_n_7\,
      Q => q_reg(12),
      R => \q_reg[0]_0\
    );
\q_reg[12]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[8]_i_1_n_0\,
      CO(3) => \q_reg[12]_i_1_n_0\,
      CO(2) => \q_reg[12]_i_1_n_1\,
      CO(1) => \q_reg[12]_i_1_n_2\,
      CO(0) => \q_reg[12]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \q_reg[12]_i_1_n_4\,
      O(2) => \q_reg[12]_i_1_n_5\,
      O(1) => \q_reg[12]_i_1_n_6\,
      O(0) => \q_reg[12]_i_1_n_7\,
      S(3 downto 0) => q_reg(15 downto 12)
    );
\q_reg[13]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[12]_i_1_n_6\,
      Q => q_reg(13),
      R => \q_reg[0]_0\
    );
\q_reg[14]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[12]_i_1_n_5\,
      Q => q_reg(14),
      R => \q_reg[0]_0\
    );
\q_reg[15]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[12]_i_1_n_4\,
      Q => q_reg(15),
      R => \q_reg[0]_0\
    );
\q_reg[16]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[16]_i_1_n_7\,
      Q => q_reg(16),
      R => \q_reg[0]_0\
    );
\q_reg[16]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[12]_i_1_n_0\,
      CO(3 downto 2) => \NLW_q_reg[16]_i_1_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \q_reg[16]_i_1_n_2\,
      CO(0) => \q_reg[16]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_q_reg[16]_i_1_O_UNCONNECTED\(3),
      O(2) => \q_reg[16]_i_1_n_5\,
      O(1) => \q_reg[16]_i_1_n_6\,
      O(0) => \q_reg[16]_i_1_n_7\,
      S(3) => '0',
      S(2 downto 0) => q_reg(18 downto 16)
    );
\q_reg[17]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[16]_i_1_n_6\,
      Q => q_reg(17),
      R => \q_reg[0]_0\
    );
\q_reg[18]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[16]_i_1_n_5\,
      Q => q_reg(18),
      R => \q_reg[0]_0\
    );
\q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[0]_i_3_n_6\,
      Q => q_reg(1),
      R => \q_reg[0]_0\
    );
\q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[0]_i_3_n_5\,
      Q => q_reg(2),
      R => \q_reg[0]_0\
    );
\q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[0]_i_3_n_4\,
      Q => q_reg(3),
      R => \q_reg[0]_0\
    );
\q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[4]_i_1_n_7\,
      Q => q_reg(4),
      R => \q_reg[0]_0\
    );
\q_reg[4]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[0]_i_3_n_0\,
      CO(3) => \q_reg[4]_i_1_n_0\,
      CO(2) => \q_reg[4]_i_1_n_1\,
      CO(1) => \q_reg[4]_i_1_n_2\,
      CO(0) => \q_reg[4]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \q_reg[4]_i_1_n_4\,
      O(2) => \q_reg[4]_i_1_n_5\,
      O(1) => \q_reg[4]_i_1_n_6\,
      O(0) => \q_reg[4]_i_1_n_7\,
      S(3 downto 0) => q_reg(7 downto 4)
    );
\q_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[4]_i_1_n_6\,
      Q => q_reg(5),
      R => \q_reg[0]_0\
    );
\q_reg[6]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[4]_i_1_n_5\,
      Q => q_reg(6),
      R => \q_reg[0]_0\
    );
\q_reg[7]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[4]_i_1_n_4\,
      Q => q_reg(7),
      R => \q_reg[0]_0\
    );
\q_reg[8]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[8]_i_1_n_7\,
      Q => q_reg(8),
      R => \q_reg[0]_0\
    );
\q_reg[8]_i_1\: unisim.vcomponents.CARRY4
     port map (
      CI => \q_reg[4]_i_1_n_0\,
      CO(3) => \q_reg[8]_i_1_n_0\,
      CO(2) => \q_reg[8]_i_1_n_1\,
      CO(1) => \q_reg[8]_i_1_n_2\,
      CO(0) => \q_reg[8]_i_1_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \q_reg[8]_i_1_n_4\,
      O(2) => \q_reg[8]_i_1_n_5\,
      O(1) => \q_reg[8]_i_1_n_6\,
      O(0) => \q_reg[8]_i_1_n_7\,
      S(3 downto 0) => q_reg(11 downto 8)
    );
\q_reg[9]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => \^sel\,
      D => \q_reg[8]_i_1_n_6\,
      Q => q_reg(9),
      R => \q_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dffr is
  port (
    lat : out STD_LOGIC;
    rst : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dffr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dffr is
begin
q_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => Q(0),
      Q => lat,
      R => rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_period_enb is
  port (
    \q_reg[3]_0\ : out STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_period_enb;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_period_enb is
  signal p_0_in : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \q[5]_i_1_n_0\ : STD_LOGIC;
  signal q_reg : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal \^q_reg[3]_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \q[1]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \q[2]_i_1\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \q[3]_i_1\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \q[4]_i_1\ : label is "soft_lutpair5";
begin
  \q_reg[3]_0\ <= \^q_reg[3]_0\;
\disp_row[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0100000000000000"
    )
        port map (
      I0 => q_reg(3),
      I1 => q_reg(2),
      I2 => q_reg(1),
      I3 => q_reg(5),
      I4 => q_reg(4),
      I5 => q_reg(0),
      O => \^q_reg[3]_0\
    );
\q[0]_i_1__0\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => q_reg(0),
      O => p_0_in(0)
    );
\q[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => q_reg(1),
      I1 => q_reg(0),
      O => p_0_in(1)
    );
\q[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => q_reg(2),
      I1 => q_reg(1),
      I2 => q_reg(0),
      O => p_0_in(2)
    );
\q[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7F80"
    )
        port map (
      I0 => q_reg(0),
      I1 => q_reg(1),
      I2 => q_reg(2),
      I3 => q_reg(3),
      O => p_0_in(3)
    );
\q[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"6AAAAAAA"
    )
        port map (
      I0 => q_reg(4),
      I1 => q_reg(0),
      I2 => q_reg(1),
      I3 => q_reg(2),
      I4 => q_reg(3),
      O => p_0_in(4)
    );
\q[5]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rst,
      I1 => \^q_reg[3]_0\,
      O => \q[5]_i_1_n_0\
    );
\q[5]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"6AAAAAAAAAAAAAAA"
    )
        port map (
      I0 => q_reg(5),
      I1 => q_reg(3),
      I2 => q_reg(2),
      I3 => q_reg(1),
      I4 => q_reg(0),
      I5 => q_reg(4),
      O => p_0_in(5)
    );
\q_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(0),
      Q => q_reg(0),
      R => \q[5]_i_1_n_0\
    );
\q_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(1),
      Q => q_reg(1),
      R => \q[5]_i_1_n_0\
    );
\q_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(2),
      Q => q_reg(2),
      R => \q[5]_i_1_n_0\
    );
\q_reg[3]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(3),
      Q => q_reg(3),
      R => \q[5]_i_1_n_0\
    );
\q_reg[4]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(4),
      Q => q_reg(4),
      R => \q[5]_i_1_n_0\
    );
\q_reg[5]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => p_0_in(5),
      Q => q_reg(5),
      R => \q[5]_i_1_n_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixel_generator is
  port (
    rgb1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    D : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AR : in STD_LOGIC_VECTOR ( 1 downto 0 );
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    \rgb2[2]\ : in STD_LOGIC_VECTOR ( 2 downto 0 );
    AS : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixel_generator;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixel_generator is
  attribute XILINX_LEGACY_PRIM : string;
  attribute XILINX_LEGACY_PRIM of \rgb1_reg[0]\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \rgb1_reg[1]\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \rgb1_reg[2]\ : label is "LDP";
  attribute XILINX_LEGACY_PRIM of \rgb2_reg[0]\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \rgb2_reg[1]\ : label is "LDC";
  attribute XILINX_LEGACY_PRIM of \rgb2_reg[2]\ : label is "LDP";
begin
\rgb1_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => AR(0),
      D => D(0),
      G => E(0),
      GE => '1',
      Q => rgb1(0)
    );
\rgb1_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => AR(1),
      D => D(1),
      G => '1',
      GE => '1',
      Q => rgb1(1)
    );
\rgb1_reg[2]\: unisim.vcomponents.LDPE
    generic map(
      INIT => '1'
    )
        port map (
      D => D(2),
      G => '1',
      GE => '1',
      PRE => AR(1),
      Q => rgb1(2)
    );
\rgb2_reg[0]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => AR(0),
      D => \rgb2[2]\(0),
      G => E(0),
      GE => '1',
      Q => rgb2(0)
    );
\rgb2_reg[1]\: unisim.vcomponents.LDCE
    generic map(
      INIT => '0'
    )
        port map (
      CLR => AR(1),
      D => \rgb2[2]\(1),
      G => '1',
      GE => '1',
      Q => rgb2(1)
    );
\rgb2_reg[2]\: unisim.vcomponents.LDPE
    generic map(
      INIT => '1'
    )
        port map (
      D => \rgb2[2]\(2),
      G => '1',
      GE => '1',
      PRE => AS(0),
      Q => rgb2(2)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowctr is
  port (
    \row_reg[2]_0\ : out STD_LOGIC;
    \col_reg[5]\ : out STD_LOGIC;
    \row_reg[2]_1\ : out STD_LOGIC;
    \row_reg[0]_0\ : out STD_LOGIC;
    \row_reg[1]_0\ : out STD_LOGIC;
    \row_reg[0]_1\ : out STD_LOGIC_VECTOR ( 0 to 0 );
    disp_row : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \rgb2_reg[0]_i_2\ : in STD_LOGIC_VECTOR ( 0 to 0 );
    \rgb2[1]\ : in STD_LOGIC;
    \rgb2[1]_0\ : in STD_LOGIC;
    rst : in STD_LOGIC;
    E : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowctr;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowctr is
  signal p_1_in : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal \row[2]_i_1_n_0\ : STD_LOGIC;
  signal \row__0\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \rgb1_reg[0]_i_4\ : label is "soft_lutpair9";
  attribute SOFT_HLUTNM of \rgb1_reg[2]_i_5\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \rgb1_reg[2]_i_6\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \rgb2_reg[0]_i_3\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \rgb2_reg[1]_i_1\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \rgb2_reg[2]_i_4\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \row[1]_i_1\ : label is "soft_lutpair10";
  attribute SOFT_HLUTNM of \row[2]_i_1\ : label is "soft_lutpair9";
begin
\disp_row_reg[0]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => E(0),
      D => \row__0\(0),
      Q => disp_row(0),
      S => rst
    );
\disp_row_reg[1]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => E(0),
      D => \row__0\(1),
      Q => disp_row(1),
      S => rst
    );
\disp_row_reg[2]\: unisim.vcomponents.FDSE
     port map (
      C => clk,
      CE => E(0),
      D => \row__0\(2),
      Q => disp_row(2),
      S => rst
    );
\rgb1_reg[0]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0001"
    )
        port map (
      I0 => \row__0\(1),
      I1 => \row__0\(0),
      I2 => \row__0\(2),
      I3 => \rgb2_reg[0]_i_2\(0),
      O => \row_reg[1]_0\
    );
\rgb1_reg[2]_i_5\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"01"
    )
        port map (
      I0 => \row__0\(2),
      I1 => \row__0\(0),
      I2 => \row__0\(1),
      O => \row_reg[2]_0\
    );
\rgb1_reg[2]_i_6\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"4000"
    )
        port map (
      I0 => \rgb2_reg[0]_i_2\(0),
      I1 => \row__0\(2),
      I2 => \row__0\(1),
      I3 => \row__0\(0),
      O => \col_reg[5]\
    );
\rgb2_reg[0]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"7FFF"
    )
        port map (
      I0 => \row__0\(2),
      I1 => \row__0\(1),
      I2 => \row__0\(0),
      I3 => \rgb2_reg[0]_i_2\(0),
      O => \row_reg[2]_1\
    );
\rgb2_reg[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"BAAAAAAA"
    )
        port map (
      I0 => \rgb2[1]\,
      I1 => \rgb2[1]_0\,
      I2 => \row__0\(0),
      I3 => \row__0\(1),
      I4 => \row__0\(2),
      O => \row_reg[0]_1\(0)
    );
\rgb2_reg[2]_i_4\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"7F"
    )
        port map (
      I0 => \row__0\(0),
      I1 => \row__0\(1),
      I2 => \row__0\(2),
      O => \row_reg[0]_0\
    );
\row[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => \row__0\(0),
      O => p_1_in(0)
    );
\row[1]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"6"
    )
        port map (
      I0 => \row__0\(1),
      I1 => \row__0\(0),
      O => p_1_in(1)
    );
\row[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"6A"
    )
        port map (
      I0 => \row__0\(2),
      I1 => \row__0\(0),
      I2 => \row__0\(1),
      O => \row[2]_i_1_n_0\
    );
\row_reg[0]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => p_1_in(0),
      Q => \row__0\(0),
      R => rst
    );
\row_reg[1]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => p_1_in(1),
      Q => \row__0\(1),
      R => rst
    );
\row_reg[2]\: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => E(0),
      D => \row[2]_i_1_n_0\,
      Q => \row__0\(2),
      R => rst
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sclk_r is
  port (
    sclk : out STD_LOGIC;
    sclk_reg_0 : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sclk_r;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sclk_r is
begin
sclk_reg: unisim.vcomponents.FDRE
     port map (
      C => clk,
      CE => '1',
      D => sclk_reg_0,
      Q => sclk,
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer_fsm is
  port (
    Q : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    rst_0 : out STD_LOGIC;
    rst_1 : out STD_LOGIC;
    SR : out STD_LOGIC_VECTOR ( 0 to 0 );
    blank_s : out STD_LOGIC;
    \FSM_onehot_state_reg[3]_0\ : in STD_LOGIC;
    \FSM_onehot_state_reg[3]_1\ : in STD_LOGIC;
    \FSM_onehot_state_reg[1]_0\ : in STD_LOGIC;
    sel : in STD_LOGIC;
    \FSM_onehot_state_reg[1]_1\ : in STD_LOGIC;
    rst : in STD_LOGIC;
    sclk : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer_fsm;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer_fsm is
  signal \FSM_onehot_state[0]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[1]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[2]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[3]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[4]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state[5]_i_1_n_0\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[0]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[1]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[3]\ : STD_LOGIC;
  signal \FSM_onehot_state_reg_n_0_[4]\ : STD_LOGIC;
  signal \^q\ : STD_LOGIC_VECTOR ( 1 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \FSM_onehot_state[2]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \FSM_onehot_state[3]_i_1\ : label is "soft_lutpair11";
  attribute SOFT_HLUTNM of \FSM_onehot_state[4]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \FSM_onehot_state[5]_i_1\ : label is "soft_lutpair13";
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[3]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[4]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[5]\ : label is "SCLK_HI:000100,BLANK0:001000,BLANK1:010000,LATCH:100000,BLANK2:000001,SCLK_LO:000010";
  attribute SOFT_HLUTNM of blank_s_INST_0 : label is "soft_lutpair13";
  attribute SOFT_HLUTNM of \col[5]_i_1\ : label is "soft_lutpair14";
  attribute SOFT_HLUTNM of \disp_row[2]_i_1\ : label is "soft_lutpair12";
  attribute SOFT_HLUTNM of \q[0]_i_1\ : label is "soft_lutpair14";
begin
  Q(1 downto 0) <= \^q\(1 downto 0);
\FSM_onehot_state[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFF080F0"
    )
        port map (
      I0 => \FSM_onehot_state_reg[1]_0\,
      I1 => sel,
      I2 => \FSM_onehot_state_reg_n_0_[0]\,
      I3 => \FSM_onehot_state_reg[3]_1\,
      I4 => \^q\(1),
      O => \FSM_onehot_state[0]_i_1_n_0\
    );
\FSM_onehot_state[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFF002A2AFF00"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[0]\,
      I1 => \FSM_onehot_state_reg[1]_0\,
      I2 => sel,
      I3 => \FSM_onehot_state_reg_n_0_[1]\,
      I4 => \FSM_onehot_state_reg[3]_1\,
      I5 => \FSM_onehot_state_reg[1]_1\,
      O => \FSM_onehot_state[1]_i_1_n_0\
    );
\FSM_onehot_state[2]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[1]\,
      I1 => \FSM_onehot_state_reg[3]_1\,
      I2 => \^q\(0),
      O => \FSM_onehot_state[2]_i_1_n_0\
    );
\FSM_onehot_state[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2F20"
    )
        port map (
      I0 => \^q\(0),
      I1 => \FSM_onehot_state_reg[3]_0\,
      I2 => \FSM_onehot_state_reg[3]_1\,
      I3 => \FSM_onehot_state_reg_n_0_[3]\,
      O => \FSM_onehot_state[3]_i_1_n_0\
    );
\FSM_onehot_state[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[3]\,
      I1 => \FSM_onehot_state_reg[3]_1\,
      I2 => \FSM_onehot_state_reg_n_0_[4]\,
      O => \FSM_onehot_state[4]_i_1_n_0\
    );
\FSM_onehot_state[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"B8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[4]\,
      I1 => \FSM_onehot_state_reg[3]_1\,
      I2 => \^q\(1),
      O => \FSM_onehot_state[5]_i_1_n_0\
    );
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[0]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[0]\,
      S => rst
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[1]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[1]\,
      R => rst
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[2]_i_1_n_0\,
      Q => \^q\(0),
      R => rst
    );
\FSM_onehot_state_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[3]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[3]\,
      R => rst
    );
\FSM_onehot_state_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[4]_i_1_n_0\,
      Q => \FSM_onehot_state_reg_n_0_[4]\,
      R => rst
    );
\FSM_onehot_state_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk,
      CE => '1',
      D => \FSM_onehot_state[5]_i_1_n_0\,
      Q => \^q\(1),
      R => rst
    );
blank_s_INST_0: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[0]\,
      I1 => \FSM_onehot_state_reg_n_0_[4]\,
      I2 => \^q\(1),
      O => blank_s
    );
\col[5]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => rst,
      I1 => \FSM_onehot_state_reg_n_0_[4]\,
      I2 => \FSM_onehot_state_reg_n_0_[3]\,
      O => SR(0)
    );
\disp_row[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \FSM_onehot_state_reg_n_0_[3]\,
      I1 => \FSM_onehot_state_reg[3]_1\,
      O => E(0)
    );
\q[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => rst,
      I1 => \^q\(1),
      O => rst_0
    );
sclk_i_1: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0000000001010100"
    )
        port map (
      I0 => rst,
      I1 => \FSM_onehot_state_reg_n_0_[4]\,
      I2 => \FSM_onehot_state_reg_n_0_[3]\,
      I3 => \^q\(0),
      I4 => sclk,
      I5 => \FSM_onehot_state_reg_n_0_[1]\,
      O => rst_1
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer is
  port (
    lat : out STD_LOGIC;
    sclk : out STD_LOGIC;
    disp_row : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AR : out STD_LOGIC_VECTOR ( 1 downto 0 );
    E : out STD_LOGIC_VECTOR ( 0 to 0 );
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    AS : out STD_LOGIC_VECTOR ( 0 to 0 );
    \col_reg[5]\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    blank_s : out STD_LOGIC;
    rst : in STD_LOGIC;
    clk : in STD_LOGIC
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer is
  signal U_COLCTR_n_0 : STD_LOGIC;
  signal U_COLCTR_n_13 : STD_LOGIC;
  signal U_COLCTR_n_8 : STD_LOGIC;
  signal U_COLCTR_n_9 : STD_LOGIC;
  signal U_HALFSCLK_n_0 : STD_LOGIC;
  signal U_ROWCTR_n_0 : STD_LOGIC;
  signal U_ROWCTR_n_1 : STD_LOGIC;
  signal U_ROWCTR_n_2 : STD_LOGIC;
  signal U_ROWCTR_n_3 : STD_LOGIC;
  signal U_ROWCTR_n_4 : STD_LOGIC;
  signal U_SEQ_n_3 : STD_LOGIC;
  signal U_SEQ_n_4 : STD_LOGIC;
  signal U_SEQ_n_5 : STD_LOGIC;
  signal col : STD_LOGIC_VECTOR ( 5 to 5 );
  signal lat_c : STD_LOGIC;
  signal rowct_enb : STD_LOGIC;
  signal \^sclk\ : STD_LOGIC;
  signal sclk_r_set : STD_LOGIC;
  signal sel : STD_LOGIC;
begin
  sclk <= \^sclk\;
U_COLCTR: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_colctr
     port map (
      AR(1 downto 0) => AR(1 downto 0),
      AS(0) => AS(0),
      D(2 downto 0) => D(2 downto 0),
      E(0) => E(0),
      \FSM_onehot_state_reg[2]\ => U_COLCTR_n_13,
      Q(0) => sclk_r_set,
      SR(0) => U_SEQ_n_5,
      clk => clk,
      \col_reg[0]_0\ => U_COLCTR_n_8,
      \col_reg[0]_1\ => U_HALFSCLK_n_0,
      \col_reg[2]_0\ => U_COLCTR_n_9,
      \col_reg[5]_0\ => U_COLCTR_n_0,
      \col_reg[5]_1\(0) => col(5),
      \col_reg[5]_2\(1) => \col_reg[5]\(2),
      \col_reg[5]_2\(0) => \col_reg[5]\(0),
      \rgb1[0]\ => U_ROWCTR_n_4,
      \rgb1[1]\ => U_ROWCTR_n_1,
      \rgb2[0]\ => U_ROWCTR_n_0,
      \rgb2[2]\ => U_ROWCTR_n_3,
      \rgb2_reg[0]_i_1_0\ => U_ROWCTR_n_2
    );
U_DFF: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_dffr
     port map (
      Q(0) => lat_c,
      clk => clk,
      lat => lat,
      rst => rst
    );
U_HALFSCLK: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_period_enb
     port map (
      clk => clk,
      \q_reg[3]_0\ => U_HALFSCLK_n_0,
      rst => rst
    );
U_REFKDLY: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_delayunit
     port map (
      clk => clk,
      \q_reg[0]_0\ => U_SEQ_n_3,
      sel => sel
    );
U_ROWCTR: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_rowctr
     port map (
      E(0) => rowct_enb,
      clk => clk,
      \col_reg[5]\ => U_ROWCTR_n_1,
      disp_row(2 downto 0) => disp_row(2 downto 0),
      \rgb2[1]\ => U_COLCTR_n_9,
      \rgb2[1]_0\ => U_COLCTR_n_8,
      \rgb2_reg[0]_i_2\(0) => col(5),
      \row_reg[0]_0\ => U_ROWCTR_n_3,
      \row_reg[0]_1\(0) => \col_reg[5]\(1),
      \row_reg[1]_0\ => U_ROWCTR_n_4,
      \row_reg[2]_0\ => U_ROWCTR_n_0,
      \row_reg[2]_1\ => U_ROWCTR_n_2,
      rst => rst
    );
U_SCLK_R: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sclk_r
     port map (
      clk => clk,
      sclk => \^sclk\,
      sclk_reg_0 => U_SEQ_n_4
    );
U_SEQ: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer_fsm
     port map (
      E(0) => rowct_enb,
      \FSM_onehot_state_reg[1]_0\ => U_ROWCTR_n_0,
      \FSM_onehot_state_reg[1]_1\ => U_COLCTR_n_13,
      \FSM_onehot_state_reg[3]_0\ => U_COLCTR_n_0,
      \FSM_onehot_state_reg[3]_1\ => U_HALFSCLK_n_0,
      Q(1) => lat_c,
      Q(0) => sclk_r_set,
      SR(0) => U_SEQ_n_5,
      blank_s => blank_s,
      clk => clk,
      rst => rst,
      rst_0 => U_SEQ_n_3,
      rst_1 => U_SEQ_n_4,
      sclk => \^sclk\,
      sel => sel
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ledmatrix_top is
  port (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    sclk : out STD_LOGIC;
    blank : out STD_LOGIC;
    lat : out STD_LOGIC;
    disp_row : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sclk_s : out STD_LOGIC;
    blank_s : out STD_LOGIC;
    lat_s : out STD_LOGIC;
    trig_s : out STD_LOGIC;
    rgb2_s : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ledmatrix_top;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ledmatrix_top is
  signal \<const0>\ : STD_LOGIC;
  signal U_SEQ_n_10 : STD_LOGIC;
  signal U_SEQ_n_11 : STD_LOGIC;
  signal U_SEQ_n_12 : STD_LOGIC;
  signal U_SEQ_n_13 : STD_LOGIC;
  signal U_SEQ_n_14 : STD_LOGIC;
  signal U_SEQ_n_5 : STD_LOGIC;
  signal U_SEQ_n_6 : STD_LOGIC;
  signal U_SEQ_n_7 : STD_LOGIC;
  signal U_SEQ_n_8 : STD_LOGIC;
  signal U_SEQ_n_9 : STD_LOGIC;
  signal \^blank_s\ : STD_LOGIC;
  signal \^lat\ : STD_LOGIC;
  signal \^rgb2\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \^sclk\ : STD_LOGIC;
begin
  blank <= \^blank_s\;
  blank_s <= \^blank_s\;
  lat <= \^lat\;
  lat_s <= \^lat\;
  rgb2(2 downto 0) <= \^rgb2\(2 downto 0);
  rgb2_s(2 downto 0) <= \^rgb2\(2 downto 0);
  sclk <= \^sclk\;
  sclk_s <= \^sclk\;
  trig_s <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
U_PIX: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_pixel_generator
     port map (
      AR(1) => U_SEQ_n_5,
      AR(0) => U_SEQ_n_6,
      AS(0) => U_SEQ_n_11,
      D(2) => U_SEQ_n_8,
      D(1) => U_SEQ_n_9,
      D(0) => U_SEQ_n_10,
      E(0) => U_SEQ_n_7,
      rgb1(2 downto 0) => rgb1(2 downto 0),
      rgb2(2 downto 0) => \^rgb2\(2 downto 0),
      \rgb2[2]\(2) => U_SEQ_n_12,
      \rgb2[2]\(1) => U_SEQ_n_13,
      \rgb2[2]\(0) => U_SEQ_n_14
    );
U_SEQ: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_sequencer
     port map (
      AR(1) => U_SEQ_n_5,
      AR(0) => U_SEQ_n_6,
      AS(0) => U_SEQ_n_11,
      D(2) => U_SEQ_n_8,
      D(1) => U_SEQ_n_9,
      D(0) => U_SEQ_n_10,
      E(0) => U_SEQ_n_7,
      blank_s => \^blank_s\,
      clk => clk,
      \col_reg[5]\(2) => U_SEQ_n_12,
      \col_reg[5]\(1) => U_SEQ_n_13,
      \col_reg[5]\(0) => U_SEQ_n_14,
      disp_row(2 downto 0) => disp_row(2 downto 0),
      lat => \^lat\,
      rst => rst,
      sclk => \^sclk\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  port (
    clk : in STD_LOGIC;
    rst : in STD_LOGIC;
    sclk : out STD_LOGIC;
    blank : out STD_LOGIC;
    lat : out STD_LOGIC;
    disp_row : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb1 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    rgb2 : out STD_LOGIC_VECTOR ( 2 downto 0 );
    sclk_s : out STD_LOGIC;
    blank_s : out STD_LOGIC;
    lat_s : out STD_LOGIC;
    trig_s : out STD_LOGIC;
    rgb2_s : out STD_LOGIC_VECTOR ( 2 downto 0 )
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is true;
  attribute CHECK_LICENSE_TYPE : string;
  attribute CHECK_LICENSE_TYPE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "design_1_ledmatrix_top_0_0,ledmatrix_top,{}";
  attribute DowngradeIPIdentifiedWarnings : string;
  attribute DowngradeIPIdentifiedWarnings of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "yes";
  attribute IP_DEFINITION_SOURCE : string;
  attribute IP_DEFINITION_SOURCE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "package_project";
  attribute X_CORE_INFO : string;
  attribute X_CORE_INFO of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix : entity is "ledmatrix_top,Vivado 2020.2";
end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture STRUCTURE of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  signal \<const0>\ : STD_LOGIC;
  signal NLW_inst_trig_s_UNCONNECTED : STD_LOGIC;
  attribute X_INTERFACE_INFO : string;
  attribute X_INTERFACE_INFO of clk : signal is "xilinx.com:signal:clock:1.0 clk CLK";
  attribute X_INTERFACE_PARAMETER : string;
  attribute X_INTERFACE_PARAMETER of clk : signal is "XIL_INTERFACENAME clk, ASSOCIATED_RESET rst, FREQ_HZ 100000000, FREQ_TOLERANCE_HZ 0, PHASE 0.0, CLK_DOMAIN /clk_wiz_0_clk_out1, INSERT_VIP 0";
  attribute X_INTERFACE_INFO of rst : signal is "xilinx.com:signal:reset:1.0 rst RST";
  attribute X_INTERFACE_PARAMETER of rst : signal is "XIL_INTERFACENAME rst, POLARITY ACTIVE_HIGH, INSERT_VIP 0";
begin
  trig_s <= \<const0>\;
GND: unisim.vcomponents.GND
     port map (
      G => \<const0>\
    );
inst: entity work.decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ledmatrix_top
     port map (
      blank => blank,
      blank_s => blank_s,
      clk => clk,
      disp_row(2 downto 0) => disp_row(2 downto 0),
      lat => lat,
      lat_s => lat_s,
      rgb1(2 downto 0) => rgb1(2 downto 0),
      rgb2(2 downto 0) => rgb2(2 downto 0),
      rgb2_s(2 downto 0) => rgb2_s(2 downto 0),
      rst => rst,
      sclk => sclk,
      sclk_s => sclk_s,
      trig_s => NLW_inst_trig_s_UNCONNECTED
    );
end STRUCTURE;
