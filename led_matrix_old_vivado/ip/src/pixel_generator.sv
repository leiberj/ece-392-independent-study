module pixel_generator (input logic [5:0] col,
                         input logic [2:0] row,
                         output logic [2:0] rgb1, rgb2);
   
   //64 columns, and 5 letters...so do 12 columns per letter
   always_comb
     begin
     //Draw an H
     if(col == 0) begin
        //top and bottom turn same color
        rgb1 = 3'b100; 
        rgb2 = 3'b100; 
     end
     else if (col > 0 && col < 12 && row == 7)begin
        rgb1 = 3'b100; 
        rgb2 = 3'b000; 
     end
     else if(col == 12) begin
        rgb1 = 3'b100; 
        rgb2 = 3'b100; 
     end
     //Draw an E
     else if(col == 14) begin
        rgb1 = 3'b010; 
        rgb2 = 3'b010; 
     end
     else if(col > 14 && col < 25 && (row == 0 || row == 7)) begin
        if(row == 0) begin
            rgb1 = 3'b010; 
            rgb2 = 3'b000;
        end
        else if(row == 7) begin
            rgb1 = 3'b010; 
            rgb2 = 3'b010; 
        end
     end
     //Draw an L
     else if(col == 26) begin
        rgb1 = 3'b001; 
        rgb2 = 3'b001; 
     end
     else if(col > 26 && col < 37 && row == 7) begin
        rgb1 = 3'b000; 
        rgb2 = 3'b001; 
     end

     //Draw an L
     else if(col == 38) begin
        rgb1 = 3'b101; 
        rgb2 = 3'b101; 
     end 
     else if(col > 37 && col < 49 && row == 7) begin
        rgb1 = 3'b000; 
        rgb2 = 3'b101; 
     end
     
     //Draw an O
     else if(col == 50) begin
        rgb1 = 3'b110;
        rgb2 = 3'b110; 
     end
     else if(col > 50 && col < 63 && (row == 0 || row == 7)) begin
        if(row == 0) begin
            rgb1 = 3'b110; 
            rgb2 = 3'd000; 
        end
        else if(row == 7) begin
            rgb1 = 3'b000; 
            rgb2 = 3'b110;
        end
     end
     else if(col == 63) begin
        rgb1 = 3'b110; 
        rgb2 = 3'b110; 
     end
     else begin
        rgb1 = 3'd0; 
        rgb2 = 3'd0; 
     end
  end // always_comb
endmodule // pixel_generator
