`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 12:21:55 PM
// Design Name: 
// Module Name: uart_xmit_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_xmit_top(input logic clk, rst, valid,
           input logic [7:0] data, 
           output logic rdy, txd);
           
    logic cont_pressed, single_pressed;
    logic valid;
    logic debounce_en; 
    
//    assign valid = cont_pressed | single_pressed;       
    
    rate_enb #(.RATE_HZ(1000)) DEBOUNCE_EN(clk, rst, 1'd0, debounce_en); 
    debouncer DEBOUNCE_1(clk, rst, debounce_en, continuous, cont_pressed);          
    single_pulse SINGLE_1(clk, rst, debounce_en, single, single_pressed);   

    uart_xmit_control_fsm CONTROLLER_1(clk, rst, valid, br_en, ct_eq_9, rdy, sh_ld, sh_idle, sh_en, br_st, ct_clr, ct_en); 
    
    uart_xmit_datapath DATAPATH_1(clk, rst, sh_ld, sh_idle, sh_en, br_st, ct_clr, ct_en, data, br_en, ct_eq_9, txd);        


endmodule
