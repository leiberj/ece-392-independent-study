`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/06/2022 08:10:49 PM
// Design Name: 
// Module Name: tx_rx_sctb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tx_rx_sim_top();

parameter CLKPD = 10;


logic clk, rst; 
logic rdy, valid, ferr, oerr; 
logic data_serial; 
logic [7:0] data_to_xmit, data_out; 


//This gets a clock generator, transmit module, receive module, and an input generating testbench
clk_gen #(.CLKPD(CLKPD)) CLKGEN(.clk(clk)); 

uart_receive_top RX_MOD(.clk, .rst, .rdy, .data(data_serial), .valid, .ferr, .oerr, .data_out); 
uart_xmit_top TX_MOD(.clk, .rst, .valid, .data(data_to_xmit), .rdy, .txd(data_serial));

tx_rx_sctb SCTB(.clk, .rst, .data_to_xmit, .valid, .rdy, .data_out); 

endmodule
