`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/06/2022 08:10:49 PM
// Design Name: 
// Module Name: tx_rx_sctb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module tx_rx_sctb(input logic clk, 
                  //Control signals to send out
                  output logic rst,
                  output logic [7:0] data_to_xmit,

                  //Inputs to check
                  input logic valid, rdy, 
                  input logic [7:0] data_out);


    parameter CLKPD = 10; 
    parameter BAUD_RATE = 9600; 
    localparam BITPD_NS = 1_000_000_000 / BAUD_RATE; //baud rate in nanoseconds

    int error_count = 0; 
    int randomVal;

    //Task to report an error and print what was wrong
    task checkResult(input logic [7:0] expected_data_output, 
                     input logic expected_valid, expected_rdy);

        if(expected_data_output != data_out) begin
            $display("Expected Data: %d, Received Data: %d", expected_data_output, data_out); 
            error_count++; 
        end
        if(expected_valid != valid) begin
            $display("Expected Valid: %d, Received Valid: %d", expected_valid, valid); 
            error_count++; 
        end
        if(expected_rdy != rdy) begin
            $display("Expected rdy: %d, Received rdy: %d", expected_rdy, rdy); 
            error_count++; 
        end

    endtask // checkResult

    //Task to display how many errors we got in running the simulation
    task reportErrors(); 
        if(error_count == 0 ) $display("No Errors");
        else $display("Simulation had %d errors", error_count); 
    endtask // reportErrors

    //Transaction tasks
    
    //To reset the DUV
    task resetDuv;
        rst = 1; 
        @(posedge clk) #1; 
        rst = 0; 
    endtask // resetDuv

    //To send a byte of data
    task sendByte(input logic [7:0] byte_out);
        //Set the data bits 
        data_to_xmit = byte_out; 
      
    endtask
    
    task transmit_bytes(input logic [7:0] n); 
        int i; 
        
        for(i = 0; i < n; i = i + 1) begin 
            randomVal = {$random} % 255; 
            sendByte(randomVal); 
            #(BITPD_NS * 10); 
        end
        
    endtask

    //To run the simulation
    initial begin
        
        $timeformat(-9, 0, "ns", 6);
        //$monitor(data, data_out, oerr, ferr, valid);
        resetDuv;
        
        //sendByte(8'haa); 
        #(BITPD_NS * 10); 
//        sendByte(8'hf0);
//        #(BITPD_NS * 20); 
        
        transmit_bytes(4); 
        
        $stop; 

    end



endmodule
