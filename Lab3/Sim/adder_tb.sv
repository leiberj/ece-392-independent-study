`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/24/2022 04:54:26 PM
// Design Name: 
// Module Name: adder_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module adder_tb();

logic clk, rst, en, clear; 
logic done; 

initial begin
    
    clk = 1'b0; 
    forever #5 clk = ~clk; 

end

adder_en_param #(.COUNT(16)) DUV(.clk, .rst, .en, .clear, .done); 

initial begin
    
rst = 1'd1; 
#15; 
rst = 1'd0; 
#15; 
en = 1'd1; 
#75; 
clear = 1'd1; 
#15; 
clear = 1'd0; 
en = 1'd0; 
#150;
en = 1'd1;  


end

endmodule
