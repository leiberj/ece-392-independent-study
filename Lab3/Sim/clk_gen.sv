`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/03/2022 07:40:43 PM
// Design Name: 
// Module Name: clk_gen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module clk_gen (output logic clk);
        parameter CLKPD = 10;
        initial begin
            assert(CLKPD >= 2) else
                $fatal("clk_gen: CLKPD must be at least 2!");
            assert(CLKPD[0] == 0) else
                $fatal("clk_gen: CLKPD must be an even number!");
            clk = 0;
            forever begin
                #(CLKPD/2) clk = ~clk;
            end 
       end
endmodule: clk_gen
 
