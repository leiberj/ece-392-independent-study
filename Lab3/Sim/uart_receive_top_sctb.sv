`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/03/2022 06:52:14 PM
// Design Name: 
// Module Name: uart_receive_top_sctb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_receive_top_sctb(input logic clk, 

                            //Outputs to control the receiver 
                             output logic rst,
                             output logic rdy,
                             output logic data,

                             //Inputs to test against
                             input logic valid, ferr, oerr,
                             input logic [7:0] data_out);

    parameter CLKPD = 10; 
    parameter BAUD_RATE = 9600; 
    localparam BITPD_NS = 1_000_000_000 / BAUD_RATE; //baud rate in nanoseconds

    //Var to keep track of how many errors we have
    int error_count = 0; 


    //Task to check values against what is expected
    task checkResult(
        input logic [7:0] expected_data,
        input logic expected_valid, expected_ferr,  expected_oerr
    );

        //Lots of if statements to check if the results don’t match what we want
        if(expected_data != data_out) begin
            $display("Expected data: %d, Received Data: %d, at t = %t", expected_data, data_out, $time); 
            error_count++; 
        end

         if(expected_valid != valid) begin
            $display("Expected valid: %h, Received valid: %h, at time %t", expected_valid, valid, $time); 
            error_count++; 
        end

         if(expected_ferr != ferr) begin
            $display("Expected ferr: %h, Received ferr: %h, at time %t", expected_ferr, ferr, $time); 
            error_count++; 
        end

         if(expected_oerr != oerr) begin
            $display("Expected oerr: %h, Received Data: %h, at time %t", expected_oerr, oerr, $time); 
            error_count++; 
        end

    endtask // checkResult


    task printResult;
        if(error_count == 0) $display("No Errors!"); 
        else $display("Test failed with %d errors", error_count); 
    endtask // printResult

    //Transaction tasks
    //To reset the DUV

    task resetDuv;
        rst = 1; 
        @(posedge clk) #1; 
        rst = 0; 
    endtask // resetDuv

    task sendPacket (logic [7:0] data_to_send); 
        rdy = 0; 
        #(BITPD_NS / 4); 
        //for loop to send 10 bits (start, data, stop)
        for(int i = 0; i < 10; i++) begin
            //if first bit send 0
            //if last bit send 1
            //otherwise send data bit
            if(i == 0) data = 0; //Start bit
            else if(i == 9) data = 1; //Stop bit
            else data = data_to_send[7 - (i-1)]; //Data bits
            
            //Wait bit cycle
            #BITPD_NS;

        end
        @(posedge clk) #1;
        checkResult(data_to_send, 1, 0, 0);
        
        //Wait some time, assert rdy (client got data)
        //Turn it off quickly
        rdy = 1; 
        @(posedge clk) #1; 
        rdy = 0; 
        
        //Check that valid has dropped, data cleared
        @(posedge clk) #1; 
        checkResult(0, 0, 0, 0); 
    endtask // sendByte


    initial begin
        
        $timeformat(-9, 0, "ns", 6);
        $monitor(data, data_out, oerr, ferr, valid);
        resetDuv;

        //Transmissions
        sendPacket(8'hAA); 
        
        
        #(BITPD_NS * 2); 
        sendPacket(8'hF0); 
        
        #(BITPD_NS * 2); 
        sendPacket(8'hC6); 
    
        #(BITPD_NS * 2); 
        sendPacket(8'h00); 
        
        #(BITPD_NS * 2); 
        sendPacket(8'hFF); 
        
        //Send 2 back to back
        #(BITPD_NS * 2); 
        sendPacket(8'b01010101); 
        sendPacket(8'b00110011); 

        printResult;

        $stop;

    end


endmodule
