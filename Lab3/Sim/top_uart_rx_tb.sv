`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/28/2022 10:30:53 AM
// Design Name: 
// Module Name: top_uart_rx_tb
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module top_uart_rx_tb();


logic clk, rst, rdy, data;
logic valid, ferr, oerr; 
logic [7:0] data_out; 

initial begin
clk = 1'b0; 
forever #1 clk = ~clk; 
end


uart_receive_top DUV(.clk, .rst, .rdy, .data, .valid, .ferr, .oerr, .data_out); 

initial begin
rst = 1'b1; 
#5; 

rst = 1'b0; 
rdy = 1'b1; 
data = 1'b1; 
#5; 

data = 1'b0; 
#50000; 

data = 1'b1; 

end

endmodule
