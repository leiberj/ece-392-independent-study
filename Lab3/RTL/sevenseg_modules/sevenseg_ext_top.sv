`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/27/2022 09:09:57 PM
// Design Name: 
// Module Name: sevenseg_ext_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevenseg_ext_top(
    input logic [6:0] data,
    output logic [7:0] an_n,
    output logic [6:0] segs_n,
    output logic dp_n
    );
    
    //Constant output on an_n to turn on last digit
    assign an_n = 8'b11111110; 
    
    //Instantiate a sevenseg_ext instance
    sevenseg_ext U_SEVENSEG_EXT_1(data, segs_n, dp_n); 
    
endmodule
