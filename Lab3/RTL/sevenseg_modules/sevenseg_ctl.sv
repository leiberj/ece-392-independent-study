`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/27/2022 09:52:37 PM
// Design Name: 
// Module Name: sevenseg_ctl
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevenseg_ctl(
    input logic clk, rst, 
    input logic [7:0] d7, d6, d5, d4, d3, d2, d1, d0, 
    output logic [6:0] segs_n, 
    output logic [7:0] an_n
    );
    
    logic [2:0] q;
    logic enb;
    logic [2:0] sel; 
    
    logic [7:0] y;
        
    rate_enb U_RATE_ENB_1(clk, rst, 1'b0, enb); 
    
    //Modules to shift which display is on
    counter U_COUNTER_1(clk, rst, enb, q); 
    dec_3_8_n U_DEC_3_8_1(q, an_n); 
    
    //What to display on each value
    mux8 #(.W(8)) U_MUX_8_1(d0, d1, d2, d3, d4, d5, d6, d7, q, y); 
    
    sevenseg_ascii SEVENSEG_ASCII(y, segs_n); 
    
endmodule
