`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/27/2022 08:25:35 PM
// Design Name: 
// Module Name: sevenseg_ext
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module sevenseg_ext(
    input logic [6:0] d, //An active high control input
    output logic [6:0] segs_n, //Control which segments to activate with an active low signal (0 turns it on)
    output logic dp_n //Control whether or not the decimal should be lit also active low
    );
    
    /*
     Seven Seg Output pattern
     A
    F B
     G
    E C
     D     
     
     order is gfedcba
v        Bit: 6543210
    */
    
    //Description of function
    //If d[6]: turn off all segments
    //Else
    //If d[5]: Turn on the decimal point
    //If d[4] display a minus
    //else display the value given
    
    always_comb begin
        //Number display logic
        if(d[6]) begin  //Blank
            segs_n = 7'b1111111; 
        end //End if
        
        else if(d[4]) begin //turn on g
            segs_n = 7'b0111111;
        end//end else if
        //We want to display a value
        else begin  
            //Lets use Prof nestor's code to take care of the rest of the cases
            case (d & 7'b0001111)//Get only the data bits     //  gfedcba
                   4'd0: segs_n = 7'b1000000;
                   4'd1: segs_n = 7'b1111001;
                   4'd2: segs_n = 7'b0100100;
                   4'd3: segs_n = 7'b0110000;
                   4'd4: segs_n = 7'b0011001;
                   4'd5: segs_n = 7'b0010010;
                   4'd6: segs_n = 7'b0000010;
                   4'd7: segs_n = 7'b1111000;
                   4'd8: segs_n = 7'b0000000;
                   4'd9: segs_n = 7'b0010000;
                   4'd10: segs_n = 7'b0001000;
                   4'd11: segs_n = 7'b0000011;
                   4'd12: segs_n = 7'b1000110; 
                   4'd13: segs_n = 7'b0100001; 
                   4'd14: segs_n = 7'b0000110;
                   4'd15: segs_n = 7'b0001110; 
                   default: segs_n = 7'b1111111;
                 endcase
        end //end else
        
        //Decimal point logic
        //If blank high turn off
        if(d[6]) begin
            dp_n = 1'b1; //Turn off
        end //end if
        
        //if dp is high turn on
        else if(d[5]) begin 
            dp_n = 1'b0; 
        end //end else if
        
        else begin //off by default
            dp_n = 1'b1; 
        end //end else
        
    end //end always_comb
    
    
    
    
endmodule
