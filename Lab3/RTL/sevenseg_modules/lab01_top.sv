`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01/27/2022 10:37:37 PM
// Design Name: 
// Module Name: lab01_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module lab01_top(
    input logic clk100MHz, rst, sel,
    output logic [7:0] an_n, 
    output logic [6:0] segs_n, 
    output logic dp_n, 
    inout tmp_scl,  //Hey those are i2c lines
    inout tmp_sda
    );
    
    logic tmp_rdy, tmp_err; //unused temp control output
    
    //13 bit two's comp resoult with 4 bit fractional part
    logic [12:0] temp;
    
    logic [11:0] temp_c; 
    logic [11:0] temp_f; 
       
    logic [3:0] temp_decimal;
    
    logic [3:0] output_letter; 
    logic [7:0] output_temp; 
    
    //instantiate VHDL temp sensor controller
    TempSensorCtl U_TSCTL (
        .TMP_SCL(tmp_scl), .TMP_SDA(tmp_sda), .TEMP_O(temp),
        .RDY_O(tmp_rdy), .ERR_O(tmp_err), .CLK_I(clk100MHz), .SRST_I(rst)
    );
    
    c_to_f U_C_TO_F(temp[11:4], temp_f); 
    
    //For celcius
    //Generate the decimal bits
    decimal_to_whole U_DECIMAL_TO_WHOLE(temp[3:0], temp_decimal); 
    
    mux_2 U_MUX_2(sel, temp[11:4], temp_f[7:0], output_letter, output_temp); 
    
    
    //We only want to send the final value of the mux to dbl_dabble
    logic[3:0] hundreds, tens, ones; 
    //Temp sensor goes to the double dabble
    dbl_dabble U_BIN_TO_BDC(output_temp, hundreds, tens, ones);
    
    //Hundreds, tens, and ones represent the bottom 3, non-c 7 seg displays (d[3], d[2], d[1])
    //Output -- hundred ten one. decimal c
    sevenseg_ctl U_SEVENSEG_CTL(clk100MHz, rst, 7'b0010000, 7'b0010000, 7'b1000000,
                                 hundreds, tens, ones | 7'b0100000, temp_decimal, output_letter, segs_n, dp_n, an_n); 
    
endmodule
