`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03/07/2022 11:08:15 AM
// Design Name: 
// Module Name: hardware_uartRX_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hardware_uartRX_top(input logic clk, rst,
                           input logic rdy,   
                           input logic serial_in, 
                           output logic valid, ferr, oerr,
                           output logic [6:0] segs_n,
                           output logic [7:0] an_n,
                           output logic [7:0] data_out);
    
    //logic [7:0] data_out; 

    uart_receive_top UART_RX(.clk, .rst, .rdy, .data(serial_in), .valid, .ferr, .oerr, .data_out); 
    
    sevenseg_ctl SEVENSEG(.clk, .rst, .d7(8'hFF), .d6(8'hFF),.d5(8'hFF),.d4(8'hFF),.d3(8'hFF),.d2(8'hFF),.d1(8'hFF),
                 .d0(data_out), .segs_n, .an_n); 
    

endmodule
