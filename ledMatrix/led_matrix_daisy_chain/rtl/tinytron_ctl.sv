module tinytron_ctl (
    input logic        clk, rst,
    output  logic      sclk, blank, lat,
    output logic [2:0] disp_row, rgb1, rgb2,
    );

   logic [2:0] row;
   logic [4:0] col;


   sequencer U_SEQ(.clk, .rst, .row, .disp_row, .col, .sclk, .blank, .lat);

   // pixel_generator U_PIX(.col, .row, .rgb1, .rgb2);

  frame_buf U_FRAMEBUF (
      .clk, .rst, .rdcol(col), .rdrow(row),
      .rgb_upper(rgb1), .rgb_lower(rgb2),
      .we_upper(1'b0), .we_lower(1'b0), .wrcol(5'b0), .din_col(32'd0)
      );

endmodule // ledmatrix_top
