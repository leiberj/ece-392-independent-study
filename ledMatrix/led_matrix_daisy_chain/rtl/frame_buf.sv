`timescale 1ns / 1ps
//-----------------------------------------------------------------------------
// Module Name   : frame_buf - BRAM frame buffer for AdaFruit LED Matrix
// Project       : ECE 212 - Digital Circuits II
//-----------------------------------------------------------------------------
// Author        : John Nestor
// Created       : 28 Jan 2022
//-----------------------------------------------------------------------------
// Description :
// This module provides a BRAM-based frame buffer for the AdaFruit
// LED Matrix.  It is organized as two frame bufffers (upper and lower)
// 3-bit pixel values are read padded with an extra unused bit
// (this is done to format BRAMs properly)
// Upper and lower frame buffers can be written one column at a time
// as 32-bit values (8 4-bit padded pixels per column)
//-----------------------------------------------------------------------------


module frame_buf(
    input logic        clk,
    input logic        rst,
    input logic [4:0]  rdpage,
    input logic [4:0]  rdcol,
    input logic [2:0]  rdrow,
    output logic [2:0] rgb_upper,
    output logic [2:0] rgb_lower,
    input logic        we_upper, we_lower,
    input logic [4:0] wrpage,
    input logic [4:0]  wrcol,
    input logic [31:0] din_col
    );
    logic [12:0] rdaddr_pix;
    assign rdaddr_pix = {rdpage,rdcol,rdrow};

    logic [9:0]  wraddr_col = {wrpage,wrcol};
    logic [3:0] we_lower_byte;  // byte write enable
    logic [3:0] we_upper_byte;  // byte write enable
    logic [3:0] dout_pix_upper, dout_pix_lower;
    assign we_lower_byte = { 4 {we_lower} };
    assign we_upper_byte = { 4 {we_upper} };
    assign rgb_upper = dout_pix_upper[2:0];
    assign rgb_lower = dout_pix_lower[2:0];

    // Upper RAM (read-only)
    BRAM_SDP_MACRO #(
    .BRAM_SIZE("36Kb"), // Target BRAM, "18Kb" or "36Kb"
    .DEVICE("7SERIES"), // Target device: "7SERIES"
    .WRITE_WIDTH(32),    // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
    .READ_WIDTH(4),     // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
    .DO_REG(0),         // Optional output register (0 or 1)
    .INIT_FILE ("NONE"),
    .SIM_COLLISION_CHECK ("ALL"), // Collision check enable "ALL", "WARNING_ONLY",
    //   "GENERATE_X_ONLY" or "NONE"
    .SRVAL(72'h000000000000000000), // Set/Reset value for port output
    .INIT(72'h000000000000000000),  // Initial values on output port
    .WRITE_MODE("WRITE_FIRST"),  // Specify "READ_FIRST" for same clock or synchronous clocks
    //   Specify "WRITE_FIRST for asynchronous clocks on ports
    .INIT_00 (256'h10000004_01226640_00000000_10006004_10006004_10006004_10006004_11226644),
    .INIT_01 (256'h10006004_10006004_10006004_11226644_00000000_01000040_10000004_10000004),
    .INIT_02 (256'h00020604_22220604_00000604_66666604_00000004_44444444_00000000_10006004),
    .INIT_03 (256'h44444444_00000004_66666604_00000604_22220604_00020604_31020604_31020604)
    /*
    .INIT_04(256'h10000004_01226640_00000000_10000004_10006004_10006004_10006004_11226644),
    .INIT_05(256'h10006004_10006004_10006004_11226644_00000000_01000040_10000004_10000004),
    .INIT_06(256'h10020004_10200004_11000040_00000000_00000000_00000000_00000000_10000004),
    .INIT_07(256'h11000040_00000000_10000000_11226644_10000040_00000000_10000640_10006004),
    .INIT_08(256'h00000000_00000000_00000000_00000000_10000640_10006004_10020004_10200004)*/
    // everything else is blank!

    ) BRAM_UPPER (
    .DO(dout_pix_upper),         // Output read data port, width defined by READ_WIDTH parameter
    .DI(din_col),         // Input write data port, width defined by WRITE_WIDTH parameter
    .RDADDR(rdaddr_pix), // Input read address, width defined by read port depth
    .RDCLK(clk),   // 1-bit input read clock
    .RDEN(1'b1),     // 1-bit input read port enable
    .REGCE(1'b0),   // 1-bit input read output register enable
    .RST(rst),       // 1-bit input reset
    .WE(we_upper_byte),         // Input write enable, width defined by write port depth
    .WRADDR(wraddr_col), // Input write address, width defined by write port depth
    .WRCLK(clk),   // 1-bit input write clock
    .WREN(1'b1)      // 1-bit input write port enable
    );

    // Lower half-matrix RAM
    BRAM_SDP_MACRO #(
    .BRAM_SIZE("36Kb"), // Target BRAM, "18Kb" or "36Kb"
    .DEVICE("7SERIES"), // Target device: "7SERIES"
    .WRITE_WIDTH(32),    // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
    .READ_WIDTH(4),     // Valid values are 1-72 (37-72 only valid when BRAM_SIZE="36Kb")
    .DO_REG(0),         // Optional output register (0 or 1)
    .INIT_FILE ("NONE"),
    .SIM_COLLISION_CHECK ("ALL"), // Collision check enable "ALL", "WARNING_ONLY",
    //   "GENERATE_X_ONLY" or "NONE"
    .SRVAL(72'h000000000000000000), // Set/Reset value for port output
    .INIT(72'h000000000000000000),  // Initial values on output port
    .WRITE_MODE("WRITE_FIRST"),  // Specify "READ_FIRST" for same clock or synchronous clocks
    //   Specify "WRITE_FIRST for asynchronous clocks on ports
    .INIT_00 (256'h10000040_00000000_00000000_10000640_10006004_10020004_10200004_11000040),
    .INIT_01 (256'h10006004_10020004_10200004_11000040_00000000_00000000_10000000_11226644),
    .INIT_02 (256'h40602000_40602222_40600000_40666666_40000000_44444444_00000000_10000640),
    .INIT_03 (256'h44444444_40000000_40666666_40600000_40602222_40602000_40602013_40602013)


    // everything else is blank!

    ) BRAM_LOWER (
    .DO(dout_pix_lower),         // Output read data port, width defined by READ_WIDTH parameter
    .DI(din_col),         // Input write data port, width defined by WRITE_WIDTH parameter
    .RDADDR(rdaddr_pix), // Input read address, width defined by read port depth
    .RDCLK(clk),   // 1-bit input read clock
    .RDEN(1'b1),     // 1-bit input read port enable
    .REGCE(1'b0),   // 1-bit input read output register enable
    .RST(rst),       // 1-bit input reset
    .WE(we_lower_byte),         // Input write enable, width defined by write port depth
    .WRADDR(wraddr_col), // Input write address, width defined by write port depth
    .WRCLK(clk),   // 1-bit input write clock
    .WREN(1'b1)      // 1-bit input write port enable
    );

endmodule
