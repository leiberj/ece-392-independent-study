module sequencer (input logic clk, rst,
                  output logic [4:0] col,      // col currently being written
                  output logic [2:0] row,      // row currently being written (connect to pixel generator)
                  output logic [2:0] disp_row, // row currently being dislayed (connect to LED matrix)
                  output logic sclk, blank, lat, trig);


   logic half_enb;          // goes high once every half a period of sclk
   logic ref_delay_start, ref_delay_done;  // used to keep the display from getting too bright
   logic sclk_r_clr, sclk_r_set, colct_clr, colct_enb, colct_eq_31, rowct_clr, rowct_enb, rowct_eq_0;


   period_enb #(.PERIOD_NS(500)) U_HALFSCLK (.clk, .rst, .clr(1'b0), .enb_out(half_enb));  // use to sequence SCLK and as timing base for everything else

   delayunit #(.DELAY_US(3000)) U_REFKDLY (.clk, .rst, .start(ref_delay_start), .done(ref_delay_done));

   logic lat_c;

// use this one for simulation
//   delayunit #(.DELAY_NS(20)) U_HALFSCLK (.clk, .rst, .done(half_enb));  // use to sequence SCLK and as timing base for everything else
//   delayunit #(.DELAY_NS(400)) U_REFKDLY (.clk, .rst(ref_delay_start || rst), .done(ref_delay_done));

   rowctr U_ROWCTR (.clk, .rst, .rowct_clr, .rowct_enb, .row, .disp_row, .rowct_eq_0);

   colctr U_COLCTR (.clk, .rst, .colct_clr, .colct_enb, .col, .colct_eq_31);

   sclk_r U_SCLK_R (.clk, .rst, .sclk_r_set, .sclk_r_clr, .sclk);

   sequencer_fsm U_SEQ (.clk, .rst, .half_enb, .colct_eq_31, .rowct_eq_0, .ref_delay_done,
                        .sclk_r_clr, .sclk_r_set, .rowct_clr,
                        .rowct_enb, .colct_clr, .colct_enb, .ref_delay_start, .blank, .lat(lat_c), .trig);

   dffr U_DFF (.clk, .rst, .d(lat_c), .q(lat));



endmodule
