`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/09/2022 01:44:35 PM
// Design Name: 
// Module Name: uart_fsm_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_fsm_sim();

logic clk, rst;
logic valid, xmit_done; 
logic [7:0] data_in;
logic ready, enable_data_out; 
logic [7:0] data_out;


//Make the clock do clock things
initial begin
	clk = 0; 
	forever #5 clk = ~clk; 
end

uart_control_FSM DUV(clk, rst, valid, xmit_done, data_in, ready, enable_data_out, data_out);


initial begin 

    rst = 1'd1; 
    #50; 
    rst = 1'd0; 
    valid = 1'd1; 
    data_in = 8'b10101010; 
    #35; 
    xmit_done = 1'd1; 
    #15; 
    xmit_done = 1'd0; 
    data_in = 8'b10001000; 
    #100; 

end

endmodule
