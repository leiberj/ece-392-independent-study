`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/09/2022 12:38:31 PM
// Design Name: 
// Module Name: uart_data_sim
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module uart_data_sim();

logic clk; 
logic enable, rst; 
logic [7:0] data; 
logic done, data_out; 

//Make the clock do clock things
initial begin
	clk = 0; 
	forever #5 clk = ~clk; 
end



uart_data_out DUV(clk, rst, enable, data, done, data_out); 

initial begin
	//Turn on reset
	rst = 1; 
	#20; 

	rst = 0; 
	enable = 0; 
	data = 8'b10101010; 
	#10; 
	enable = 1; 
	#150; 
	enable = 0; 
	data = 8'b10001000; 
	#20; 
	enable = 1; 
	#20; 

end


endmodule
