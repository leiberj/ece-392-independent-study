`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 02/18/2022 12:02:46 PM
// Design Name: 
// Module Name: datapath
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module datapath(input logic clk, rst, 
                input logic sh_ld, sh_idle, sh_en,
                input logic br_st,
                input logic ct_clr, ct_en,
                input logic [7:0] data,
                output logic br_en, ct_eq_9, txd); 

    parameter baudRate = 9600; 
    
    //Baud rate enable sig
    rate_enb #(.RATE_HZ(baudRate)) RATE_EN_1(clk, rst, br_st, br_en); 
    
    count10 COUNTER_1(clk, rst, ct_clr, ct_en, ct_eq_9); 
    
    shift_reg SHIFT_REG_1(clk, rst, sh_ld, sh_idle, sh_en, 1'd0, 1'd1, data, txd); 
    
endmodule
