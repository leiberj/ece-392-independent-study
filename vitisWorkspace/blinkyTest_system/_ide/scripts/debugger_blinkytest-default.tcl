# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: D:\Users\Owner\Documents\Senior_Spring\Independent_Study\ece_392_independent_study\vitisWorkspace\blinkyTest_system\_ide\scripts\debugger_blinkytest-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source D:\Users\Owner\Documents\Senior_Spring\Independent_Study\ece_392_independent_study\vitisWorkspace\blinkyTest_system\_ide\scripts\debugger_blinkytest-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -filter {jtag_cable_name =~ "Digilent Nexys A7 -100T 210292AE241BA" && level==0 && jtag_device_ctx=="jsn-Nexys A7 -100T-210292AE241BA-13631093-0"}
fpga -file D:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/vitisWorkspace/blinkyTest/_ide/bitstream/design_1_wrapper.bit
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
loadhw -hw D:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/vitisWorkspace/design_1_wrapper/export/design_1_wrapper/hw/design_1_wrapper.xsa -regs
configparams mdm-detect-bscan-mask 2
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
rst -system
after 3000
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
dow D:/Users/Owner/Documents/Senior_Spring/Independent_Study/ece_392_independent_study/vitisWorkspace/blinkyTest/Debug/blinkyTest.elf
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
con
