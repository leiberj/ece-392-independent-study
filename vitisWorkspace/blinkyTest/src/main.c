#include "xparameters.h"
#include "xil_printf.h"
#include "xgpio.h"
#include "xil_types.h"

// Get device IDs from xparameters.h
#define BTN_ID XPAR_AXI_GPIO_BUTTONS_DEVICE_ID
#define LED_ID XPAR_AXI_GPIO_LED_DEVICE_ID
#define RGB_ID XPAR_AXI_GPIO_RGB_DEVICE_ID
#define BTN_CHANNEL 1
#define LED_CHANNEL 1
#define RGB_CHANNEL 1
#define BTN_MASK 0b1111
#define LED_MASK 0xFFFF
#define RGB_MASK 0b11

#define redBlue 0b100010

int main() {
	XGpio_Config *cfg_ptr;
	XGpio led_device, btn_device, rgb_device;
	u32 data;

	xil_printf("Entered function main\r\n");

	// Initialize LED Device
	cfg_ptr = XGpio_LookupConfig(LED_ID);
	XGpio_CfgInitialize(&led_device, cfg_ptr, cfg_ptr->BaseAddress);

	// Initialize Button Device
	cfg_ptr = XGpio_LookupConfig(BTN_ID);
	XGpio_CfgInitialize(&btn_device, cfg_ptr, cfg_ptr->BaseAddress);

	//Init rgb
	cfg_ptr = XGpio_LookupConfig(RGB_ID);
	XGpio_CfgInitialize(&rgb_device, cfg_ptr, cfg_ptr->BaseAddress);

	// Set Button Tristate
	XGpio_SetDataDirection(&btn_device, BTN_CHANNEL, BTN_MASK);

	// Set Led Tristate (SET THEM ALL TO BE OUTPUTS)
	XGpio_SetDataDirection(&led_device, LED_CHANNEL, 0);

	//Set RGB Tristate
	XGpio_SetDataDirection(&rgb_device, RGB_CHANNEL, 0);

	while (1) {

		XGpio_DiscreteWrite(&rgb_device, RGB_CHANNEL, redBlue);

		data = XGpio_DiscreteRead(&btn_device, BTN_CHANNEL);
		data &= BTN_MASK;
		if (data != 0) {
			data = LED_MASK;
		} else {
			data = 0;
		}
		XGpio_DiscreteWrite(&led_device, LED_CHANNEL, data);
	}
}
