
#include "xparameters.h"
#include "xil_printf.h"
#include "xil_types.h"
#include "inverter.h"
#include "xil_io.h"
#include "sleep.h"


#define INVERT_ID XPAR_INVERTER_0_DEVICE_ID
#define INVERT_CHANNEL 1

void main(){
	int i;
	u8 data = 0;
	xil_printf("Entered function main\r\n");

	INVERTER_Reg_SelfTest(XPAR_INVERTER_0_INVERT_AXI_BASEADDR);

	while(1){
		xil_printf("data is: %d\n\r", data);
		INVERTER_mWriteReg(XPAR_INVERTER_0_INVERT_AXI_BASEADDR, INVERTER_INVERT_AXI_SLV_REG0_OFFSET, data );
		data = (~data & 0x01);

		sleep(1);
	}
}
