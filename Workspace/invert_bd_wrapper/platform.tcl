# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct D:\Xilinx\Workspace\invert_bd_wrapper\platform.tcl
# 
# OR launch xsct and run below command.
# source D:\Xilinx\Workspace\invert_bd_wrapper\platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {invert_bd_wrapper}\
-hw {D:\Users\Owner\Documents\Senior_Spring\Independent_Study\ece_392_independent_study\inverter_test\invert_bd_wrapper.xsa}\
-fsbl-target {psu_cortexa53_0} -out {D:/Xilinx/Workspace}

platform write
domain create -name {standalone_microblaze_0} -display-name {standalone_microblaze_0} -os {standalone} -proc {microblaze_0} -runtime {cpp} -arch {32-bit} -support-app {empty_application}
platform generate -domains 
platform active {invert_bd_wrapper}
platform generate -quick
platform generate
