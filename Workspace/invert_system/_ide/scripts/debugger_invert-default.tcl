# Usage with Vitis IDE:
# In Vitis IDE create a Single Application Debug launch configuration,
# change the debug type to 'Attach to running target' and provide this 
# tcl script in 'Execute Script' option.
# Path of this script: D:\Xilinx\Workspace\invert_system\_ide\scripts\debugger_invert-default.tcl
# 
# 
# Usage with xsct:
# To debug using xsct, launch xsct and run below command
# source D:\Xilinx\Workspace\invert_system\_ide\scripts\debugger_invert-default.tcl
# 
connect -url tcp:127.0.0.1:3121
targets -set -filter {jtag_cable_name =~ "Digilent Nexys A7 -100T 210292AE241BA" && level==0 && jtag_device_ctx=="jsn-Nexys A7 -100T-210292AE241BA-13631093-0"}
fpga -file D:/Xilinx/Workspace/invert/_ide/bitstream/invert_bd_wrapper.bit
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
loadhw -hw D:/Xilinx/Workspace/invert_bd_wrapper/export/invert_bd_wrapper/hw/invert_bd_wrapper.xsa -regs
configparams mdm-detect-bscan-mask 2
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
rst -system
after 3000
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
dow D:/Xilinx/Workspace/invert/Debug/invert.elf
targets -set -nocase -filter {name =~ "*microblaze*#0" && bscan=="USER2" }
con
