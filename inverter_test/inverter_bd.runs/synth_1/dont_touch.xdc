# This file is automatically generated.
# It contains project source information necessary for synthesis and implementation.

# XDC: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/constrain/nexys_a7_100t_master_constrain.xdc

# Block Designs: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/invert_bd.bd
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd || ORIG_REF_NAME==invert_bd} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_clk_wiz_0_0/invert_bd_clk_wiz_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_clk_wiz_0_0 || ORIG_REF_NAME==invert_bd_clk_wiz_0_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_reset_inv_0_0/invert_bd_reset_inv_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_reset_inv_0_0 || ORIG_REF_NAME==invert_bd_reset_inv_0_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_microblaze_0_0/invert_bd_microblaze_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_microblaze_0_0 || ORIG_REF_NAME==invert_bd_microblaze_0_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_dlmb_v10_0/invert_bd_dlmb_v10_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_dlmb_v10_0 || ORIG_REF_NAME==invert_bd_dlmb_v10_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_ilmb_v10_0/invert_bd_ilmb_v10_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_ilmb_v10_0 || ORIG_REF_NAME==invert_bd_ilmb_v10_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_dlmb_bram_if_cntlr_0/invert_bd_dlmb_bram_if_cntlr_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_dlmb_bram_if_cntlr_0 || ORIG_REF_NAME==invert_bd_dlmb_bram_if_cntlr_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_ilmb_bram_if_cntlr_0/invert_bd_ilmb_bram_if_cntlr_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_ilmb_bram_if_cntlr_0 || ORIG_REF_NAME==invert_bd_ilmb_bram_if_cntlr_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_lmb_bram_0/invert_bd_lmb_bram_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_lmb_bram_0 || ORIG_REF_NAME==invert_bd_lmb_bram_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_mdm_1_0/invert_bd_mdm_1_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_mdm_1_0 || ORIG_REF_NAME==invert_bd_mdm_1_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_rst_clk_wiz_0_100M_0/invert_bd_rst_clk_wiz_0_100M_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_rst_clk_wiz_0_100M_0 || ORIG_REF_NAME==invert_bd_rst_clk_wiz_0_100M_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_axi_uartlite_0_0/invert_bd_axi_uartlite_0_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_axi_uartlite_0_0 || ORIG_REF_NAME==invert_bd_axi_uartlite_0_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_xbar_0/invert_bd_xbar_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_xbar_0 || ORIG_REF_NAME==invert_bd_xbar_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_microblaze_0_axi_periph_0/invert_bd_microblaze_0_axi_periph_0.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_microblaze_0_axi_periph_0 || ORIG_REF_NAME==invert_bd_microblaze_0_axi_periph_0} -quiet] -quiet

# IP: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_inverter_0_1/invert_bd_inverter_0_1.xci
set_property KEEP_HIERARCHY SOFT [get_cells -hier -filter {REF_NAME==invert_bd_inverter_0_1 || ORIG_REF_NAME==invert_bd_inverter_0_1} -quiet] -quiet

# XDC: C:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/invert_bd_ooc.xdc
