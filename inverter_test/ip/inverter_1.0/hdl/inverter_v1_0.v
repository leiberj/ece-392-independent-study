
`timescale 1 ns / 1 ps

	module inverter_v1_0 #
	(
		// Users to add parameters here

		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface INVERT_AXI
		parameter integer C_INVERT_AXI_DATA_WIDTH	= 32,
		parameter integer C_INVERT_AXI_ADDR_WIDTH	= 4
	)
	(
		// Users to add ports here
        output wire inv_out,
        output wire input_sig,
		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface INVERT_AXI
		input wire  invert_axi_aclk,
		input wire  invert_axi_aresetn,
		input wire [C_INVERT_AXI_ADDR_WIDTH-1 : 0] invert_axi_awaddr,
		input wire [2 : 0] invert_axi_awprot,
		input wire  invert_axi_awvalid,
		output wire  invert_axi_awready,
		input wire [C_INVERT_AXI_DATA_WIDTH-1 : 0] invert_axi_wdata,
		input wire [(C_INVERT_AXI_DATA_WIDTH/8)-1 : 0] invert_axi_wstrb,
		input wire  invert_axi_wvalid,
		output wire  invert_axi_wready,
		output wire [1 : 0] invert_axi_bresp,
		output wire  invert_axi_bvalid,
		input wire  invert_axi_bready,
		input wire [C_INVERT_AXI_ADDR_WIDTH-1 : 0] invert_axi_araddr,
		input wire [2 : 0] invert_axi_arprot,
		input wire  invert_axi_arvalid,
		output wire  invert_axi_arready,
		output wire [C_INVERT_AXI_DATA_WIDTH-1 : 0] invert_axi_rdata,
		output wire [1 : 0] invert_axi_rresp,
		output wire  invert_axi_rvalid,
		input wire  invert_axi_rready
	);
// Instantiation of Axi Bus Interface INVERT_AXI
	inverter_v1_0_INVERT_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_INVERT_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_INVERT_AXI_ADDR_WIDTH)
	) inverter_v1_0_INVERT_AXI_inst (
		.S_AXI_ACLK(invert_axi_aclk),
		.S_AXI_ARESETN(invert_axi_aresetn),
		.S_AXI_AWADDR(invert_axi_awaddr),
		.S_AXI_AWPROT(invert_axi_awprot),
		.S_AXI_AWVALID(invert_axi_awvalid),
		.S_AXI_AWREADY(invert_axi_awready),
		.S_AXI_WDATA(invert_axi_wdata),
		.S_AXI_WSTRB(invert_axi_wstrb),
		.S_AXI_WVALID(invert_axi_wvalid),
		.S_AXI_WREADY(invert_axi_wready),
		.S_AXI_BRESP(invert_axi_bresp),
		.S_AXI_BVALID(invert_axi_bvalid),
		.S_AXI_BREADY(invert_axi_bready),
		.S_AXI_ARADDR(invert_axi_araddr),
		.S_AXI_ARPROT(invert_axi_arprot),
		.S_AXI_ARVALID(invert_axi_arvalid),
		.S_AXI_ARREADY(invert_axi_arready),
		.S_AXI_RDATA(invert_axi_rdata),
		.S_AXI_RRESP(invert_axi_rresp),
		.S_AXI_RVALID(invert_axi_rvalid),
		.S_AXI_RREADY(invert_axi_rready),
		
		.inv_out(inv_out),
		.input_sig(input_sig)
	);

	// Add user logic here

	// User logic ends

	endmodule
