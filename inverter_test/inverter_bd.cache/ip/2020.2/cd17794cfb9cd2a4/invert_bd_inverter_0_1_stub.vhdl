-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
-- Date        : Mon Apr 25 10:41:20 2022
-- Host        : ECE419-92PZ0Q2 running 64-bit major release  (build 9200)
-- Command     : write_vhdl -force -mode synth_stub -rename_top decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix -prefix
--               decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix_ invert_bd_inverter_0_1_stub.vhdl
-- Design      : invert_bd_inverter_0_1
-- Purpose     : Stub declaration of top-level module interface
-- Device      : xc7a100tcsg324-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
  Port ( 
    inv_out : out STD_LOGIC;
    input_sig : out STD_LOGIC;
    invert_axi_aclk : in STD_LOGIC;
    invert_axi_aresetn : in STD_LOGIC;
    invert_axi_awaddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    invert_axi_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    invert_axi_awvalid : in STD_LOGIC;
    invert_axi_awready : out STD_LOGIC;
    invert_axi_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
    invert_axi_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
    invert_axi_wvalid : in STD_LOGIC;
    invert_axi_wready : out STD_LOGIC;
    invert_axi_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    invert_axi_bvalid : out STD_LOGIC;
    invert_axi_bready : in STD_LOGIC;
    invert_axi_araddr : in STD_LOGIC_VECTOR ( 3 downto 0 );
    invert_axi_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
    invert_axi_arvalid : in STD_LOGIC;
    invert_axi_arready : out STD_LOGIC;
    invert_axi_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
    invert_axi_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
    invert_axi_rvalid : out STD_LOGIC;
    invert_axi_rready : in STD_LOGIC
  );

end decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix;

architecture stub of decalper_eb_ot_sdeen_pot_pi_dehcac_xnilix is
attribute syn_black_box : boolean;
attribute black_box_pad_pin : string;
attribute syn_black_box of stub : architecture is true;
attribute black_box_pad_pin of stub : architecture is "inv_out,input_sig,invert_axi_aclk,invert_axi_aresetn,invert_axi_awaddr[3:0],invert_axi_awprot[2:0],invert_axi_awvalid,invert_axi_awready,invert_axi_wdata[31:0],invert_axi_wstrb[3:0],invert_axi_wvalid,invert_axi_wready,invert_axi_bresp[1:0],invert_axi_bvalid,invert_axi_bready,invert_axi_araddr[3:0],invert_axi_arprot[2:0],invert_axi_arvalid,invert_axi_arready,invert_axi_rdata[31:0],invert_axi_rresp[1:0],invert_axi_rvalid,invert_axi_rready";
attribute X_CORE_INFO : string;
attribute X_CORE_INFO of stub : architecture is "inverter_v1_0,Vivado 2020.2";
begin
end;
