//Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
//Date        : Mon Apr 25 10:40:24 2022
//Host        : ECE419-92PZ0Q2 running 64-bit major release  (build 9200)
//Command     : generate_target invert_bd_wrapper.bd
//Design      : invert_bd_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module invert_bd_wrapper
   (input_sig,
    inv_out,
    reset,
    sys_clock,
    usb_uart_rxd,
    usb_uart_txd);
  output input_sig;
  output inv_out;
  input reset;
  input sys_clock;
  input usb_uart_rxd;
  output usb_uart_txd;

  wire input_sig;
  wire inv_out;
  wire reset;
  wire sys_clock;
  wire usb_uart_rxd;
  wire usb_uart_txd;

  invert_bd invert_bd_i
       (.input_sig(input_sig),
        .inv_out(inv_out),
        .reset(reset),
        .sys_clock(sys_clock),
        .usb_uart_rxd(usb_uart_rxd),
        .usb_uart_txd(usb_uart_txd));
endmodule
