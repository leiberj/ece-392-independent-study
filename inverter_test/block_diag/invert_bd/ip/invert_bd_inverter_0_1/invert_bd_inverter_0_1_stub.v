// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.2 (win64) Build 3064766 Wed Nov 18 09:12:45 MST 2020
// Date        : Mon Apr 25 10:41:20 2022
// Host        : ECE419-92PZ0Q2 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               c:/Users/leiberj/Documents/independent_study/ece-392-independent-study/inverter_test/block_diag/invert_bd/ip/invert_bd_inverter_0_1/invert_bd_inverter_0_1_stub.v
// Design      : invert_bd_inverter_0_1
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* X_CORE_INFO = "inverter_v1_0,Vivado 2020.2" *)
module invert_bd_inverter_0_1(inv_out, input_sig, invert_axi_aclk, 
  invert_axi_aresetn, invert_axi_awaddr, invert_axi_awprot, invert_axi_awvalid, 
  invert_axi_awready, invert_axi_wdata, invert_axi_wstrb, invert_axi_wvalid, 
  invert_axi_wready, invert_axi_bresp, invert_axi_bvalid, invert_axi_bready, 
  invert_axi_araddr, invert_axi_arprot, invert_axi_arvalid, invert_axi_arready, 
  invert_axi_rdata, invert_axi_rresp, invert_axi_rvalid, invert_axi_rready)
/* synthesis syn_black_box black_box_pad_pin="inv_out,input_sig,invert_axi_aclk,invert_axi_aresetn,invert_axi_awaddr[3:0],invert_axi_awprot[2:0],invert_axi_awvalid,invert_axi_awready,invert_axi_wdata[31:0],invert_axi_wstrb[3:0],invert_axi_wvalid,invert_axi_wready,invert_axi_bresp[1:0],invert_axi_bvalid,invert_axi_bready,invert_axi_araddr[3:0],invert_axi_arprot[2:0],invert_axi_arvalid,invert_axi_arready,invert_axi_rdata[31:0],invert_axi_rresp[1:0],invert_axi_rvalid,invert_axi_rready" */;
  output inv_out;
  output input_sig;
  input invert_axi_aclk;
  input invert_axi_aresetn;
  input [3:0]invert_axi_awaddr;
  input [2:0]invert_axi_awprot;
  input invert_axi_awvalid;
  output invert_axi_awready;
  input [31:0]invert_axi_wdata;
  input [3:0]invert_axi_wstrb;
  input invert_axi_wvalid;
  output invert_axi_wready;
  output [1:0]invert_axi_bresp;
  output invert_axi_bvalid;
  input invert_axi_bready;
  input [3:0]invert_axi_araddr;
  input [2:0]invert_axi_arprot;
  input invert_axi_arvalid;
  output invert_axi_arready;
  output [31:0]invert_axi_rdata;
  output [1:0]invert_axi_rresp;
  output invert_axi_rvalid;
  input invert_axi_rready;
endmodule
